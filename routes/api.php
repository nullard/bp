<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('logout', 'LoginController@logoutJWT');

Route::get('login/verify','LoginController@verifyJWT');
Route::get('home/{type?}/{user_unique?}','HomeController@index');

Route::group(['prefix'=> 'notice'], function() {
    $Controller = 'NoticeController';
    Route::get('/',$Controller.'@index')->middleware('auth:front');
    Route::post('other/read',$Controller.'@otherRead')->middleware('auth:front');
    Route::post('my/read',$Controller.'@myRead')->middleware('auth:front');
    Route::post('all/read',$Controller.'@allRead')->middleware('auth:front');
    Route::any('nutri/index',$Controller.'@statisticForNutritionist');//->middleware('auth:front');
});

Route::group(['prefix'=> 'like'],function() {
    $Controller = 'LikeController';
    Route::get('/',$Controller.'@index');
    Route::get('show/{unique}',$Controller.'@show');
    Route::post('save',$Controller.'@save')->middleware('auth:front');
});

Route::group(['prefix'=> 'comment'],function() {
    $Controller = 'CommentController';
    Route::get('/',$Controller.'@index');
    Route::post('delete',$Controller.'@delete');
    Route::post('save',$Controller.'@save')->middleware('auth:front');
});

Route::group(['prefix'=> 'track'], function(){
    $Controller = 'TrackController';
    Route::get('list/{user_unique?}',$Controller.'@index');
    Route::post('approve',$Controller.'@approve')->middleware('auth:front');
    Route::get('{unique}/{is_track}',$Controller.'@track');
});

Route::group(['prefix'=> 'member'], function() {
    $Controller = 'MemberController';
    Route::get('show',$Controller.'@show');
    Route::post('privacy/save',$Controller.'@privacySave');
    Route::post('god_vw/save',$Controller.'@godVwSave');
    Route::post('save',$Controller.'@save');
    Route::get('search/{keyword}',$Controller.'@search');
    Route::get('diet/list/{unique?}/{date?}',$Controller.'@dietList');
    Route::get('{unique?}',$Controller.'@index');
});


Route::group(['prefix' => 'drecord'],function() {
    Route::post('save','MemberDrecordController@save');
    Route::get('{unique?}','MemberDrecordController@index');
});

Route::group(['prefix' => 'diet'],function() {

    Route::get('show/{unique}','DietController@show');
    Route::post('save','DietController@save');
    Route::post('rate/{unique}','DietController@rate');
    Route::post('read/{unique}','DietController@read');
    Route::post('privacy','DietController@setPrivacy')->middleware('auth:front');
    Route::post('delete','DietController@delete')->middleware('auth:front');
});

Route::group(['prefix' => 'chart'],function() {
//    Route::get('daily/{unique}/{date}','ChartController@daily');
    Route::get('week/{unique}/{date}','ChartController@week');
    Route::get('month/{unique}/{date}','ChartController@month');
    Route::get('six-month/{unique}/{date}','ChartController@sixMonth');
});


Route::group(['prefix'=>'mobile'],function() {

    Route::post('login','LoginController@loginFromMobile');
    Route::post('setLogin','LoginController@setLoginFromMobile');
    Route::post('signup','LoginController@signupFromMobile');
    Route::post('forgot','LoginController@forgotPasswordFromMobile');
    Route::post('send/active','LoginController@sendActive');

});

//Test
/*
Route::get('updateDrecord/24621850',function() {
    $items = App\MemberDrecord::where('target_cal',0)->get();

    foreach($items as $item) {

        $o = App\MemberCalTarget::find($item->member_cal_target_id);
        if ($o) {
            $item->target_cal = $o->cal;
            $item->save();
        }
    }

});*/


