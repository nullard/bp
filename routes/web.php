<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test/{search}',function($search) {

    /*
    $exitCode = Artisan::queue('diet:caloria', [
        'food' => $search
    ]);*/
/*
    $exitCode = Artisan::call('diet:caloria', [
        'food' => $search
    ]);*/


    return '';
});

Route::get('logout', 'LoginController@logoutJWT');
Route::get('login/{social_type}/callback', 'LoginController@handleProviderCallback')->where(['social_type' => '(facebook|line)']);
Route::get('login/{social_type}/{recommand?}', 'LoginController@redirectToProvider')->where(['social_type' => '(facebook|line)']);

Route::get('active/{code}/{token}','LoginController@active');
Route::post('change/password','LoginController@setPassword');
Route::get('change/password/{code}/{token}','LoginController@changePassword');


Route::group(['prefix'=>'admin'],function() {

    Route::get('/', function() {
        return redirect(route('login'));
    });

    Route::any('logout','Auth\LoginController@logout');
    Auth::routes();

    Route::group(['prefix' => 'member','middleware' => 'auth'],function() {
        Route::get('edit/{id}','Admin\MemberController@edit')->name('admin.members.edit');
        Route::get('show/{id}','Admin\MemberController@show')->name('admin.members.show');
        Route::post('store','Admin\MemberController@store')->name('admin.members.store');
        Route::get('destroy/{member}','Admin\MemberController@destroy')->name('admin.members.destroy');
        Route::get('{sdate?}','Admin\MemberController@index')->name('admin.members');
    });

    Route::group(['prefix'=>'recommand'],function() {
        Route::get('/','Admin\RecommandController@index')->name('admin.recommand');
        Route::get('edit/{id?}','Admin\RecommandController@edit')->name('admin.recommand.edit');
        Route::get('show/{id}','Admin\RecommandController@show')->name('admin.recommand.show');
        Route::post('store','Admin\RecommandController@store')->name('admin.recommand.store');
    });

});



Route::any('/{all}', function () {
    return view('layouts.app');
})->where(['all' => '.*']);




//Route::get('/home', 'HomeController@index')->name('home');
