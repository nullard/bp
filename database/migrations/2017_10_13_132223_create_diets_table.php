<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDietsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diets', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('meal_time');
            $table->integer('member_id');
            $table->integer('message_id');
            $table->integer('protein')->default(0)->nullable()->comment('蛋白質');
            $table->integer('fat')->default(0)->nullable()->comment('脂肪');
            $table->integer('sugar')->default(0)->nullable()->comment('糖');
            $table->string('photo');
            $table->string('name')->nullable();
            $table->string('type',10)->comment('caloria;weight')->nullable();
            $table->string('value')->nullable();
            $table->integer('user_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diets');
    }
}
