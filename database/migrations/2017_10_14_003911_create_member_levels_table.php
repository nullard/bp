<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateMemberLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //曹狀結構上下線關係
        Schema::create('member_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->unique();
            $table->string('name',100)->nullable();
            $table->timestamps();
            $table->softDeletes();

            NestedSet::columns($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_levels');
    }
}
