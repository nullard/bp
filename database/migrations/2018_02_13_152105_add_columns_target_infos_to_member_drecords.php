<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTargetInfosToMemberDrecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_drecords', function (Blueprint $table) {
            $table->integer('target_cal')->defalut(0)->after('cal');
            $table->text('suggestions')->nullable()->after('target_cal');
            $table->text('tasks')->nullable()->after('suggestions');

            $table->float('weight')->default(0)->change();
            $table->integer('member_kg_target_id')->default(0)->change();
            $table->integer('member_cal_target_id')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_drecords', function (Blueprint $table) {
            $table->dropColumn('target_cal');
            $table->dropColumn('suggestions');
            $table->dropColumn('tasks');


        });
    }
}
