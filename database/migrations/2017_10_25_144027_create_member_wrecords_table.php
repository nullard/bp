<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberWrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_wrecords', function (Blueprint $table) {
            $table->increments('id');
            $table->date('sdate');
            $table->date('edate');
            $table->integer('member_id');
            $table->text('suggestion')->nullable();
            $table->text('next_task')->nullable();
            $table->text('data')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_wrecords');
    }
}
