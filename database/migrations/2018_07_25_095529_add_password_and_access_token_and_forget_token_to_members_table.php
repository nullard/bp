<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPasswordAndAccessTokenAndForgetTokenToMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->string('password',255)->nullable();
            $table->string('access_token',255)->nullable();
            $table->string('access_utc')->nullable();
            $table->string('forget_token',255)->nullable();
            $table->string('forget_utc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('access_token');
            $table->dropColumn('access_utc');
            $table->dropColumn('forget_token');
            $table->dropColumn('forget_utc');
        });
    }
}
