<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsgReadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msg_reads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('chatroom_id');
            $table->integer('unread_count')->default(0);
            $table->integer('read_message_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msg_reads');
    }
}
