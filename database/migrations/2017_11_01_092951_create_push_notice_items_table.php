<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushNoticeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('push_notice_id');
            $table->string('name')->nullable();
            $table->string('type')->comment('uri,message,postback');
            $table->string('desp')->nullable();
            $table->integer('seq')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notice_items');
    }
}
