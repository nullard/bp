<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('pay_type')->default('credit_period');
            $table->char('period_type')->default('M');
            $table->integer('frequency')->default(1);
            $table->integer('exec_times')->default(12);
            $table->integer('total_success_times')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('pay_type');
            $table->dropColumn('period_type');
            $table->dropColumn('frequency');
            $table->dropColumn('exec_times');
            $table->dropColumn('total_success_times');
        });
    }
}
