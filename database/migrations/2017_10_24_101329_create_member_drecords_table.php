<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberDrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_drecords', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->date('sdate');
            $table->float('weight');
            $table->integer('cal');
            $table->integer('member_kg_target_id');
            $table->integer('member_cal_target_id');
            $table->tinyInteger('over_cal')->comment('卡路里超標')->default(0)->nullable();
            $table->integer('over_diet_id')->comment('關聯運動照片')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_drecords');
    }
}
