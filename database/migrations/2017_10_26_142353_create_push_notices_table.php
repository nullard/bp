<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notices', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(0);
            $table->string('name');
            $table->string('role_type')->comment('member,nutritionist,counselor');
            $table->string('type')->comment('line,mail,text(簡訊)');
            $table->string('message_type')->nullable()->comment('text,carousel');
            $table->text('message');
            $table->string('params')->nullable()->comment('替換message參數');
            $table->string('time_type')->comment('everyday,everyweek,everymonth,once');
            $table->string('time_desp')->nullable()->comment('幾月幾號或星期幾');
            $table->string('time');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notices');
    }
}
