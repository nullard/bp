<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDpToMemberDrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_drecords', function (Blueprint $table) {
            $table->integer('sbp')->default(0)->after('cal');
            $table->integer('dbp')->default(0)->after('sbp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_drecords', function (Blueprint $table) {
            $table->dropColumn('sbp');
            $table->dropColumn('dbp');
        });
    }
}
