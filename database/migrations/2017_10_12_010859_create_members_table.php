<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('code',30)->unique();
            $table->string('name',50);
            $table->string('real_name',100)->nullable();
            $table->string('email')->nullable();
            $table->date('birth')->nullable();
            $table->tinyInteger('sex')->default(-1);
            $table->integer('pay')->default(0)->comment('月費付多少');
            $table->tinyInteger('status')->default(0);
            $table->string('level',50)->nullable();
            $table->string('type',10)->comment('line');
            $table->tinyInteger('vegetarian')->default(0)->comment('素食主義');
            $table->string('social_id',100);
            $table->string('pic')->nullable();
            $table->integer('last_message_id')->default(0);
            $table->date('pay_sdate')->nullable();
            $table->date('pay_edate')->nullable();
            $table->integer('recommand_id')->default(0)->nullable();


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
