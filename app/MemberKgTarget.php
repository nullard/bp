<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberKgTarget extends Model
{
    protected $fillable = [
        'member_id', 'tall','weight'
    ];
}
