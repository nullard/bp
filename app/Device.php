<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Device extends Model
{
    protected $fillable = ['member_id','type','push_token','active'];

    public function member()
    {
        $this->belongsTo('App\Member');
    }

    public function checkIsExist($member_id,$type,$token)
    {
        if ($type == '' || $token == '') {
            return ;
        }

        $count = self::where('member_id', $member_id)
            ->where('type',$type)
            ->where('push_token',$token)
            ->count();

        if ($count > 0) {
            $this->active($member_id,$type,$token);
        }else {
            $this->add($member_id,$type,$token);
        }
    }

    public function active($member_id,$type,$token)
    {
        $row = self::where('member_id',$member_id)
            ->where('type',$type)
            ->where('push_token',$token)
            ->first();

        if ($row) {
            if ($row->active == 0) {
                $row->active = 1;
                $row->update();
            }
        }


    }


    public function inactive($member_id)
    {
        $rows = self::where('member_id',$member_id)->get();

        if ( $rows ) {
            foreach ($rows as $row) {
                if ($row->active == 1) {
                    $row->active = 0;
                    $row->update();
                }
            }

        }


    }

    public function add($member_id,$type,$token)
    {
        self::create([
           'member_id' => $member_id,
            'type'  => $type,
            'push_token'    => $token,
            'active'    => 1
        ]);
    }
}
