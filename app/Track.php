<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Track extends Model
{
    use SoftDeletes;
    protected $fillable = ['member_id','track_member_id','approve'];

    static public function trackListArray($member_id)
    {
        $trackList = self::select('track_member_id')->where('member_id',$member_id)->get();

        return $trackList->pluck('track_member_id')->toArray();
    }
}

