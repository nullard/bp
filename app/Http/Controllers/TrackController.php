<?php

namespace App\Http\Controllers;

use App\Member;
use App\Notifications\MyDynamic;
use App\Track;
use App\Transforms\MemberTransform;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Collection;

class TrackController extends Controller
{
    public function index($user_unique=null)
    {
        $rows = [];

        if ($user_unique) {
            $user = Member::where('code',$user_unique)->first();
        }else {
            $user = Auth::guard('front')->user();
        }


        if ($user) {
            $collecion = new Collection();
            $trackArray = Track::trackListArray($user->id);
            if (!empty($trackArray)) {
                $collecion = Member::whereIn('id',$trackArray)->get();
            }

            $rows['data'] = (new MemberTransform())->transform($collecion);
        }


        return response()->json($rows);

    }

    public function track($user_unique,$is_track)
    {
        $user = Auth::guard('front')->user();


        if ($user) {

            $oTrack = new Track();

            $oMember = new Member();
            $rowMember = $oMember->where('code',$user_unique)->first();

            if (isset($rowMember) && $user->id != $rowMember->id) {


                if ($is_track) {

                    $o = $oTrack->firstOrCreate([
                        'member_id'         => $user->id,
                        'track_member_id'   => $rowMember->id,
                    ]);

                    if ($o->wasRecentlyCreated) {
                        $rowMember->notify(new MyDynamic($user,'track', $o->id));
                    }

                }else {

                    if ($user->level == "patient") {
                        if ($user->parent_id == $rowMember->id) {
                            return response()->json(["status" => 403]);
                        }
                    }else if($user->level == "nutritionist") {
                        if ($user->id == $rowMember->parent_id) {
                            return response()->json(["status" => 403]);
                        }
                    }

                    $o = Track::where('member_id',$user->id)->where('track_member_id',$rowMember->id)->first();
                    $approve = $o->approve;
                    $relative_id = $o->id;
                    $o->delete();
                    if ($approve != -1) {
                        $rowMember->notify(new MyDynamic($user,'untrack', $relative_id));
                    }

                }

            }
        }

    }

    function approve(Request $request) {

        $oFrom = Member::getDataByCode($request->from);
        $oTo = Auth::guard('front')->user();

        $row = Track::where('member_id',$oFrom->id)->where('track_member_id',$oTo->id)->first();

        if ($row) {

            if ($row->approve == 0) {
                $row->approve = $request->approve;
                $row->save();

                if ($row->approve == 1) {
                    $oFrom->notify(new MyDynamic($oTo,'track_approve', $row->id));
                }else {
                    $oFrom->notify(new MyDynamic($oTo,'track_deny', $row->id));
                }

            }


        }

    }
}
