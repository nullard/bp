<?php

namespace App\Http\Controllers;

use App\Device;
use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use App\Mail\NewMemberActive;
use App\Member;
use App\MemberDrecord;
use App\Recommand;
use App\Services\LineService;
use App\Services\So88Upload;
use App\Services\UploadService;
use App\Track;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\MemberKgTarget;
use Socialite;
use Auth;
use JWTAuth;
use Config;
use Illuminate\Support\Facades\Log;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }


    public function redirectToProvider($social_type,$recommand=null)
    {
        $device_type = Input::get('device_type');
        $device_token = Input::get('device_token');

        Session::put('DeviceType',$device_type);
        Session::put('DeviceToken',$device_token);
        Session::put('Recommand',$recommand);

        if ($social_type == 'line') {
            $client_id = Config('services.line.client_id');
            $redirect_uri = urlencode(Config('services.line.redirect'));
            $state = 'so88comtw';
            $scope = urlencode('openid profile');
            $auth_link = 'https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id='.$client_id.'&redirect_uri='.$redirect_uri.'&state='.$state.'&scope='.$scope;

            return redirect($auth_link);
        }else {
            return Socialite::driver($social_type)->redirect();
        }


    }

    public function handleProviderCallback($social_type)
    {

        try {

            $recommand = Session::has('Recommand') ? Session::get('Recommand') : '';

            switch($social_type) {
                case 'facebook':

                    $user = Socialite::driver($social_type)->user();
                    $name = isset($user->user['nickname']) ? $user->user['nickname'] : $user->user['name'];
                    $real_name = $user->user['name'];

                    $sex = -1;
                    $social_id = $user->user['id'];
                    $email = isset($user->user['email']) ? $user->user['email'] : null;
                    $pic = $user->avatar_original;
                    break;
                case 'line':

                    $code = Input::get('code');

                    if (isset($code)) {

                        $result = (new LineService())->requestLineServer($code);
                        if ($result['status'] == 'OK') {
                            $output = (new LineService())->paserJWTFromLineResponse($result['data']);

                            $user = $output['user'];

                            if ($output['status'] == 'OK') {
                                $name = isset($user->user['nickname']) ? $user->user['nickname'] : $user->user['name'];
                                $real_name = $user->user['name'];
                                $sex = -1;
                                $social_id = $user->user['id'];
                                $email = $user->user['email'];
                                $pic = $user->avatar_original;
                            }

                        }else {
//                            return view('errors.undefined',compact('output'));
                        }

                    }
                    break;
            }
        }catch (\Exception $e) {
            print("Sorry, Login Fail".$e->getMessage());
            return redirect('/');
        }

        $device_type = Session::has('DeviceType') ? Session::get('DeviceType') : '';
        $device_token = Session::has('DeviceToken') ? Session::get('DeviceToken') : '';

        $member_count = Member::where('type',$social_type)->where('social_id',$social_id)->count();

        if ($member_count == 0) {

            $oMember = $this->createMember($recommand,[
                'name'        => $name,
                'real_name'   => $real_name,
                'sex'         => $sex,
                'type'        => $social_type,
                'social_id'   => $social_id,
                'email'       => $email,
                'pic'         => $pic
            ],$device_type,$device_token);

            $this->frontLogin($oMember);

            return redirect('/signup');
        }else {
            $oDevice = new Device();
            $oMember = Member::with('latestKgTarget')->where('type',$social_type)->where('social_id',$social_id)->first();
            $oDevice->checkIsExist($oMember->id,$device_type,$device_token);

            $this->frontLogin($oMember);

            $info = $oMember->latestKgTarget;
            if (isset($info[0])) {
                if (isset($info[0]['tall']) && isset($info[0]['weight'])) {
                    return redirect('/');
                }
            }

            return redirect('/signup');
        }

    }

    function createMember($recommand,$data,$device_type,$device_token) {

        $oDevice = new Device();
        $code =  mt_rand(0,9).Carbon::now()->timestamp.mt_rand(100,999);

        if (isset($recommand)) {
            $oRecommand = Recommand::where('qrcode',$recommand)->first();
        }

        $status = in_array($data['type'],['facebook','line']) ? 1 : 0;

        $oMember = Member::create([
            'parent_id'   => (isset($oRecommand) && isset($oRecommand->member_id) && $oRecommand->member_id > 0) ? $oRecommand->member_id : 0,
            'code'        => $code,
            'name'        => $data['name'],
            'real_name'   => $data['real_name'],
            'sex'         => $data['sex'],
            'type'        => $data['type'],
            'social_id'   => $data['type'] == 'so88' ? $code : $data['social_id'],
            'email'       => isset($data['email']) ? $data['email'] : null,
            'status'      => $status,
            'level'       => 'patient',
            'recommand_id'  => isset($oRecommand) ? $oRecommand->id : 0,
            'password'      => isset($data['password']) ? $data['password'] : null
        ]);

        if ($data['type'] == 'so88') {
            $oMember->active_token = str_replace("/", "-", Hash::make($oMember->code.Str::random(5).$oMember->id.Str::random(32)));
            $oMember->active_utc = time();
            $oMember->save();
        }


        $oDevice->checkIsExist($oMember->id,$device_type,$device_token);
        /* 確認推薦人是否建立帳號*/
        if (isset($oRecommand)) {
            if (isset($oRecommand->member_id) && $oRecommand->member_id > 0) {
                Track::create([
                    'member_id' => $oMember->id,
                    'track_member_id' => $oRecommand->member_id,
                    'approve' => 1
                ]);
                Track::create([
                    'member_id' => $oRecommand->member_id,
                    'track_member_id' => $oMember->id,
                    'approve' => 1
                ]);
            }
        }

        if (!empty($data['pic'])) {
            $relativePath = '/profiles/'.$oMember->id.'/';
            $filename = (new UploadService)->fromURL($data['pic'], So88Upload::rootPath().$relativePath, 200);

            $filePath = $relativePath.$filename;

            $oMember->pic = $filePath;
            $oMember->save();
        }

        return $oMember;
    }

    protected function frontLogin($oMember) {

        if (isset($oMember)) {
            $id = $oMember->id;
            $user = Auth::guard('front')->loginUsingId($id,true);

            $custom = $this->jwtCustomColumns($user);

            $token = JWTAuth::fromUser($user,$custom);

//            Log::info("user id ".$user->id);

            return [
                "status" => 200,
                'token'  => $token
            ];
        }else {
            $this->frontLogout();
        }

        return ["status" => 400];
    }

    function frontLogout() {
        Auth::guard('front')->logout();
    }

    function verifyJWT(Request $request) {

        if (Auth::guard('front')->check()) {
            $user = Auth::guard('front')->user();


            $custom = $this->jwtCustomColumns($user);

            \Config::set('auth.providers.users.model', \App\Member::class);
            $token = JWTAuth::fromUser($user,$custom);
            $status = 200;

        }else {
            $status = 400;
            $token = '';
        }

        return response()->json([
            'status'    => $status,
            'token'     => $token
        ]);
    }

    function jwtCustomColumns($user) {

        $oKgTarget = MemberKgTarget::where('member_id',$user->id)->latest()->first();

        $weight = '';
        $tall = '';
        if ($oKgTarget) {
            $tall = $oKgTarget->tall;
            $weight = $oKgTarget->weight;
        }


        $custom = [
            'notice_badge' => count($user->unreadNotifications),
            'name'  => $user->name,
            'role'  => $user->level,
            'photo' => storageURL($user->pic),
            'id'    => $user->code,
            'desp'  => $user->desp,
            'tall'  => $tall,
            'weight' => $weight,
            'post_share_type' => $user->post_share_type,
            'is_admin' => $user->is_admin,
            'god_vw' => $user->god_vw,
            'recommand' => !empty($user->recommand_id) ? Recommand::find($user->recommand_id)->name : ''
        ];

        return $custom;
    }

    function logoutJWT(Request $request) {

        $user = Auth::guard('front')->user();

        $isApp = $request->ajax();


        if ($user) {
            $oDevice = new Device();
            if ( $isApp ) {
                $oDevice->inactive($user->id);
            }

            Auth::guard('front')->logout();
        }

        if ($isApp) {
            return response()->json([]);
        }else {
            return redirect('/');
        }

    }


    function loginFromMobile(Request $request) {

        //if ($request->api_token == config('services.so88.client_secret')) {

            $device_type= $request->device_type;
            $device_token = $request->device_token;

            $access_token = $request->access_token;
            $email = $request->email;
            $type = $request->login_type;
            $social_id = $request->social_id;
            $pic = $request->picture;
            $name = $request->name;

            $password = $request->password;

            $isSocial = in_array($type,["line","facebook"]);

            if ( $isSocial ) {
                $oMember = Member::where('type',$type)->where('social_id',$social_id)->first();
            }else {
                $oMember = Member::where('type',$type)->where('email',$email)->first();
            }


            if ($oMember) {

                $oDevice = new Device();
                $oDevice->checkIsExist($oMember->id,$device_type,$device_token);

                $code = 401;
                $msg = 'OK';
                if ( $isSocial ) {
                    $code = 200;
                }else {
                    if ((Hash::check($password, $oMember->password))) {
                        if ($oMember->status == 1) {
                            $code = 200;
                        }else {
                            $code = 405;// 尚未啟用
                            $msg = 'Email not active';
                        }
                    }
                }

                return response()->json([
                    'code' => $code,
                    'msg' => $msg,
                    'data' => $code == 200 ? ['code' => $oMember->code, 'access_token'=> $this->updateAcessToken($oMember,$access_token)] : null
                ]);
            }else {

                if ( $isSocial ) {

                    $oMember = $this->createMember('',[
                        'name'        => $name,
                        'real_name'   => $name,
                        'sex'         => -1,
                        'type'        => $type,
                        'social_id'   => $social_id,
                        'email'       => $email,
                        'pic'         => $pic
                    ],$device_type,$device_token);

                    return response()->json(['code' => 200,
                                             'msg' => 'ok',
                                             'data' => ['code' => $oMember->code, 'access_token'=> $this->updateAcessToken($oMember,$access_token)]]);
                }else {
                    return response()->json([
                        'code' => 401,
                        'msg' => '[2]email or password error',
                    ]);
                }
            }

        //}


        return response()->json(['code'=>401,'msg'=>'not authorize']);
    }

    protected function updateAcessToken($oMember,$old_access_token) {

        $access_token = $oMember->access_token;

        if ($access_token == $old_access_token && isset($access_token)) {

            $oMember->access_utc = time();
            $oMember->save();

        }else {
            $access_token = str_replace("/", "-", Hash::make($oMember->code.Str::random(5).$oMember->id.Str::random(32)));
            $oMember->access_token = $access_token;
            $oMember->access_utc = time();
            $oMember->save();
        }

        return $access_token;
    }

    function signupFromMobile(Request $request) {

        $device_type = $request->device_type;
        $device_token = $request->device_token;
        $email = $request->email;
        $password = $request->password;

        $count = Member::where('type','so88')->where('email',$email)->count();

        if ($count == 0) {
            $oMember = $this->createMember('',[
                'name'        => '',
                'real_name'   => '',
                'sex'         => -1,
                'type'        => 'so88',
                'social_id'   => '',
                'email'       => $email,
                'pic'         => '',
                'password'    => Hash::make($password)
            ],$device_type,$device_token);

            Mail::to($oMember->email)->later(Carbon::now()->addSecond(1), new NewMemberActive($oMember));

            return response()->json([
                'code'    => 200,
            ]);
        }else {
            return response()->json([
                'code'    => 201,
                'msg'   => 'email had been used'
            ]);
        }



    }

    function forgotPasswordFromMobile(Request $request) {

        $email = $request->email;
        $oMember = Member::where('status',1)->where('type','so88')->where('email',$email)->first();

        $t = time();

        if ( $oMember ) {

            if ( $oMember->forget_token == null || $oMember->forget_utc < $t ) {
                $oMember->forget_token = str_replace("/", "-",Hash::make($oMember->code.Str::random(5).$oMember->id.Str::random(32)));
                $oMember->forget_utc = Carbon::now()->addHours(3)->timestamp;
                $oMember->save();
            }


            Mail::to($oMember->email)->later(Carbon::now()->addSecond(1), new ForgotPassword($oMember));

        }

        return response()->json([
            'code'    => 200,
            'msg' => isset($oMember->name) ? true : false
        ]);

    }

    function changePassword($code,$token)
    {

        $oMember = Member::where('code',$code)->where('forget_token',$token)->first();
        $t = time();


        if ( $oMember ) {

            $msg = null;

            if ( isset($oMember->forget_token) && $oMember->forget_utc > $t) {
            } else {
                //請重新申請變更密碼請求
                $msg = "網址有錯誤或者是超過修改期限，請重新申請忘記密碼作業，謝謝";
            }


            return view('change_password',compact('oMember','msg'));
        }else {

            //成為不存在
            return redirect('/');

        }

    }

    function setPassword(Request $request) {

        $oMember = Member::where('forget_token',$request->token)->first();
        $password = $request->password;
        $t = time();

        if ( $oMember ) {

            if ( $oMember->forget_utc > $t && strlen($password) >= 8) {

                $oMember->password = Hash::make($password);
                $oMember->forget_token = null;
                $oMember->forget_utc = null;
                $oMember->save();

                $request->session()->flash('status', '密碼變更完成，確認後轉跳首頁');
            }else {
                $request->session()->flash('status', '密碼變更有誤');
            }
        }else {
            $request->session()->flash('status', '找不到該帳號');
        }



        return view('change_password',compact('oMember'));

    }

    function setLoginFromMobile(Request $request) {

        $access_token = $request->access_token;
        $code = $request->code;


        $oMember = Member::where('access_token', $access_token)->where('code', $code)->first();


//        Log::info(isset($oMember) ? 'OK' : 'Fail');

        return response()->json($this->frontLogin($oMember));

    }

    function sendActive(Request $request) {

        $oMember = Member::where('type','so88')->where('email',$request->email)->first();

        Mail::to($oMember->email)->later(Carbon::now()->addSecond(1), new NewMemberActive($oMember));

        return response()->json([
            'code'    => 200,
        ]);
    }

    function active($code,$token) {

        $oMember = Member::where('code',$code)->first();

        if ($oMember->status == 0) {
            if ( $oMember->active_token == $token ) {
                $oMember->status = 1;
                $oMember->save();
                return "瘦88帳戶已啟用，我們一起來減重吧";
            }
            return "瘦88帳戶啟用失敗，請確認您的連結是否有複製正確，謝謝";
        }else {
            return redirect("/");
        }
    }
}
