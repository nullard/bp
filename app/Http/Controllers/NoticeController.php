<?php

namespace App\Http\Controllers;

use App\Diet;
use App\Member;
use App\Transforms\NotificationTransform;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class NoticeController extends Controller
{
    public function index()
    {

        $user = Auth::guard('front')->user();


        $other_dynamic_count = 45;
        $my_dynamic_count = 45;

        $other_dynamic = $user->unreadNotifications->where('type','App\Notifications\OtherDynamic');
        $other_dynamic_remain = $other_dynamic_count - count($other_dynamic);
        $rs_other_dynamic = (new NotificationTransform())->transformOther($other_dynamic);

        $rs_other_dynamic_read = [];
        if ($other_dynamic_remain > 0) {
            $other_dyanmic_read = $user->notifications->where('read_at','!=',null)->where('type','App\Notifications\OtherDynamic')->take($other_dynamic_remain);
            $rs_other_dynamic_read = (new NotificationTransform())->transformOther($other_dyanmic_read,1);
        }


        $my_dynamic = $user->unreadNotifications->where('type','App\Notifications\MyDynamic');

        $my_dynamic_remain = $my_dynamic_count - count($my_dynamic);
        if ($my_dynamic_remain > 0) {
            $read = $user->notifications->where('read_at','!=',null)->where('type','App\Notifications\MyDynamic')->take($my_dynamic_remain);
            $my_dynamic = $my_dynamic->merge($read);
        }
        $rs_my_dynamic = (new NotificationTransform())->transformMy($my_dynamic,$user);


        $rows['other_dynamic'] = $rs_other_dynamic;
        $rows['other_dynamic_read'] = $rs_other_dynamic_read;
        $rows['my_dynamic'] = $rs_my_dynamic;

        return response()->json($rows);

    }

    public function otherRead(Request $request)
    {
        $user = Auth::guard('front')->user();

        $oMember = Member::where('code',$request->user_id)->first();

        $types = explode(',',$request->type);

        if (is_array($types)) {
            $user->unreadNotifications->where('type','App\Notifications\OtherDynamic')
                ->where('data.user_id',$oMember->id)
                ->whereIn('data.type',$types)
                ->markAsRead();
        }else {
            $user->unreadNotifications->where('type','App\Notifications\OtherDynamic')
                ->where('data.user_id',$oMember->id)
                ->where('data.type',$request->type)
                ->markAsRead();
        }


        return response()->json([]);

    }

    public function myRead() {

        $user = Auth::guard('front')->user();

        $total = count($user->unreadNotifications);
        $read = count($user->unreadNotifications->where('type','App\Notifications\MyDynamic'));
        $user->unreadNotifications->where('type','App\Notifications\MyDynamic')->markAsRead();


        return response()->json(["badge" => $total - $read, "read" => $read]);
    }

    public function allRead()
    {
        $user = Auth::guard('front')->user();

        $user->unreadNotifications->where('type','App\Notifications\OtherDynamic')->markAsRead();

        return response()->json([]);
    }

    public function statisticForNutritionist(Request $request)
    {
        $unique = $request->unique;
        $o = Member::where('code',$unique)->first();

        $rows = [];
        if ($o->level == 'nutritionist') {

            $members = Member::with('parent')->where('parent_id',$o->id)->get();
            $rows = $members->reject(function($member) {

                $sdate = $member->pay_sdate.' 00:00:00';
                $count = Diet::notRateCountWithFood($member->id,$sdate);

                return $count == 0 ? true : false;

            })->map(function($member) {

                $sdate = $member->pay_sdate.' 00:00:00';
                $count = Diet::notRateCountWithFood($member->id,$sdate);

                return [
                    'user_img'  => storageURL($member->pic),
                    'user_name' => $member->name,
                    'user_unique'   => $member->code,
                    'relative_unique' => $count > 1 ? $member->code : (Diet::getNotRateData($member->id,$sdate))->id,
                    'parent_unique' => $member->parent->code,
                    'type'      => 'caloria',
                    'badge'     => $count,
                    'message_count' => $count,
                    'diff_time' => Carbon::parse($member->created_at)->diffForHumans()
                ];

            });

            return response()->json(array_values($rows->all()));
        }else {
            return response()->json($rows);
        }



    }
}
