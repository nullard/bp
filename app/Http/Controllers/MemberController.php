<?php

namespace App\Http\Controllers;

use App\Diet;
use App\Member;
use App\MemberDrecord;
use App\MemberKgTarget;
use App\Notifications\MyDynamic;
use App\Recommand;
use App\Services\So88Upload;
use App\Services\UploadService;
use App\Track;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\Transforms\MemberTransform;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;


class MemberController extends Controller
{
    public function index($unique = null)
    {

        $o = Member::with('parent')->where('code',$unique)->first();

        if ($o) {
            $row['role'] = $o->level;
            $row['name'] = $o->name;
            $row['unique'] = $o->code;
            $row['parent_unique'] = isset($o->parent) ? $o->parent->code : 0;
            $row['image'] = storageURL($o->pic);
            $row['desp'] = $o->desp;
            $row['track'] = -99;

            $user = Auth::guard('front')->user();
            if ($user) {
                $oTrack = Track::where('member_id',$user->id)
                    ->where('track_member_id',$o->id)->first();
                if ($oTrack) {
                    $row['track'] = $oTrack->approve;
                }

            }
            $initWeight = 0;

            $oInitWeight = MemberDrecord::where('member_id',$o->id)
                ->where('weight','>', 0)
                ->oldest()
                ->first();

            if (!$oInitWeight) {
                if (isset($o->latestKgTarget[0])) {
                    $initWeight = $o->latestKgTarget[0]['weight'];
                }
            }else {
                $initWeight = $oInitWeight->weight;
            }

            $oRecentWeight = MemberDrecord::where('member_id',$o->id)
                ->where('weight','>', 0)
                ->latest()
                ->first();

            $lossWeight = 0;
            if($oRecentWeight) {
                $recentWeight = $oRecentWeight->weight;
                $lossWeight = round($recentWeight  - $initWeight,2);
            }

            $row['weight']['start'] = $initWeight;
            $row['weight']['end']   = isset($oRecentWeight) ? $oRecentWeight->weight : 0;
            $row['loss_weight']['status'] = $lossWeight > 0 ? '+' : '-';
            $row['loss_weight']['weight'] = abs($lossWeight);
        }else {
            $row = [];
        }

        return response()->json($row);

    }

    public function save(Request $request) {

        $user = Auth::guard('front')->user();

        if ($user) {

            $validation = [
                'id'    => 'required|unique:members,code,'.$user->id.'|max:255',
                'name'  => 'required',
                'tall'  => 'required|numeric|min:50|max:300',
                'weight' => 'required|numeric|min:35|max:260'
            ];

//            if ($request->type == 'register') {}

            if ($request->recommand != "") {
                Validator::extend('recommands_exists', function($attribute, $value, $parameters)
                {
                    return DB::table($parameters[0])
                            ->where($parameters[1], $value)
                            ->orWhere($parameters[2], $value)
                            ->count() > 0;
                });

                $validation = array_merge($validation, [
//                        'recommand' => 'exists:recommands,qrcode'
                    'recommand' => 'recommands_exists:recommands,qrcode,name'

                ]);
            }



            $validator = Validator::make($request->all(), $validation);

            if ($validator->fails()) {
                $result = [
                    'status' => 'FAIL',
                    'data' => $validator->errors()
                ];
                return response()->json($result);
            }


            $user->name = isset($request->name) ? $request->name : $user->name;
            $user->code = isset($request->id) ? $request->id : $user->code;
            $user->birth = isset($request->birth) ? $request->birth : $user->birth;
            $user->desp = isset($request->desp) ? $request->desp : $user->desp;

            if ($user->desp == 'null') {
                $user->desp = '';
            }

            if (isset($request->photo)){
                $relativePath = '/profiles/'.$user->id.'/';
                $filename = (new UploadService)->fromBase64($request->photo,So88Upload::rootPath().$relativePath,600);

                $user->pic = $relativePath.$filename;
            }

            $user->save();

            if ($request->recommand != "" && $user->recommand_id == 0) {


                $oRecommand = Recommand::where('qrcode',$request->recommand)->orWhere('name',$request->recommand)->first();

                if ($oRecommand) {
                    $user->recommand_id = $oRecommand->id;

                    $user->save();

                    //推播通知
                    if (isset($oRecommand->member_id) && $user->parent_id == 0) {
                        $rowMember = Member::find($oRecommand->member_id);

                        if ($rowMember && $rowMember->id != $user->id) {

                            $this->eachOtherTracking($user,$rowMember);

                            $user->parent_id = $oRecommand->member_id;
                            $user->save();


                        }
                    }

                }
            }


            //if ($request->type == 'register') {
                $oKgTarget = MemberKgTarget::where('member_id',$user->id)->latest()->first();
                if (!$oKgTarget) {
                    $oKgTarget = new MemberKgTarget();
                    $oKgTarget->weight = 0;
                }
                $oKgTarget->member_id = $user->id;

                $oKgTarget->tall = $request->tall;
                $oKgTarget->weight = $request->weight;

                $oKgTarget->save();
            //}




        }

        return response()->json(['status'=>'OK']);

    }

    //民眾跟專家 專家跟民眾 相互追蹤
    private function eachOtherTracking($user,$rowMember)
    {

        $this->handleTracking($user,$rowMember);
        $this->handleTracking($rowMember,$user);

    }

    private function handleTracking($tracking,$tracked)
    {

        $checkTracking = Track::where('member_id',$tracking->id)->where('track_member_id',$tracked->id)->count();

        if ($checkTracking == 0) {

            $create_tracking = Track::create([
                'member_id' => $tracking->id,
                'track_member_id' => $tracked->id,
                'approve' => 1
            ]);
            $tracked->notify(new MyDynamic($tracking,'track', $create_tracking->id));

        }else if($checkTracking == 1){

            $oTrack = Track::where('member_id',$tracking->id)->where('track_member_id',$tracked->id)->first();

            if ($oTrack->approve != 1) {
                $oTrack->approve = 1;
                $oTrack->save();
            }

        }else {
            Track::where('member_id',$tracking->id)->where('track_member_id',$tracked->id)->delete();

            Track::create([
                'member_id' => $tracking->id,
                'track_member_id' => $tracked->id,
                'approve' => 1
            ]);
        }

    }

    public function show()
    {
        $user = Auth::guard('front')->user();

        $result['name'] = $user->name;
        $result['id'] = $user->code;
        $result['photo'] = storageURL($user->pic);
        $result['birth'] = $user->birth;
        $result['desp'] = $user->desp;

        $weight = '';
        $tall = '';
        $oKgTarget = MemberKgTarget::where('member_id',$user->id)->latest()->first();
        if ($oKgTarget) {
            $tall = $oKgTarget->tall;
            $weight = $oKgTarget->weight;
        }
        $result['weight'] = $weight;
        $result['tall'] = $tall;
        $result['recommand'] = '';

        if($user->recommand_id > 0) {
            $oRecommand = Recommand::find($user->recommand_id);
            if (isset($oRecommand)) {
                $result['recommand'] = $oRecommand->name;
            }
        }


        return response()->json($result);
    }

    public function search($keyword)
    {
        $collecion = Member::where(function($query) use($keyword){
            $query->where('code', 'LIKE', '%'.$keyword.'%');
            $query->orWhere('name', 'LIKE', '%'.$keyword.'%');
        })->limit(20)->get();

        $rows['data'] = (new MemberTransform())->transform($collecion);

        return response()->json($rows);
    }

    public function privacySave(Request $request)
    {
        $user = Auth::guard('front')->user();

        if ($user) {

            $o = Member::find($user->id);
            $o->post_share_type = $request->post_share_type;
            $o->update();

            return $o->post_share_type;
        }

    }

    public function godVwSave(Request $request)
    {
        $user = Auth::guard('front')->user();

        if ($user && $user->is_admin == 1) {

            $o = Member::find($user->id);
            $o->god_vw = $request->god_vw;
            $o->update();

            return $o->god_vw;
        }
    }

    public function dietList($unique = null, $date = null) {

        $user = Auth::guard('front')->user();
        $o = Member::where('code',$unique)->first();

//        sleep(3);

        //確認是否是本人或者是專屬營養師
        $who = 'other';
        $isCheckUser = false;
        $isRate = false;
        if ($user) {

            if ($user->id == $o->id) {
                $isCheckUser = true;
                $who = 'myself';
            }else {
                if ($user->level == 'nutritionist') {
                    if ($user->id == $o->parent_id) {
                        $isCheckUser = true;
                        $isRate = true;
                        $who = 'nutritionist';
                    }
                }
            }

        }

        $imageCount = 0;
        $imageMaxCount = 25;
        $registe_date = Carbon::parse($o->created_at)->format("Y-m-d");
        if ($date == null) {
            $date = Carbon::now()->format("Y-m-d");
        }



        $count = Diet::dietShare($user)
            ->where('member_id',$o->id)
            ->where('meal_time','<=',$date." 23:59:59")
            ->where('photo','!=','')->count();

        $rows = [];
        $end_date = $date;

        if ($count > 0) {

            $dateCount = 0;

            //統計比數
            while($imageCount < $imageMaxCount){
                $date_count =  Diet::dietShare($user)
                    ->where('member_id',$o->id)
                    ->where('meal_time','>=',$end_date." 00:00:00")
                    ->where('meal_time','<=',$end_date." 23:59:59")
                    ->where('photo','!=','')->count();

                $imageCount += $date_count;

                $end_date = Carbon::parse($end_date)->addDay(-1)->format("Y-m-d");
                $dateCount++;

                if ($count <= $imageCount) {
                    break;
                }


                if ($date_count == 0) {
                    if($end_date < $registe_date) {
                        $end_date = null;
                        break;
                    }
                }
            }

            //撈出資料
            for($i=0; $i<=$dateCount; $i++) {
                $search_date = Carbon::parse($date)->addDay($i*-1)->format("Y-m-d");

                $oDiet =  Diet::dietShare($user)
                    ->where('member_id',$o->id)
                    ->where('meal_time','LIKE',$search_date."%")
                    ->where('photo','!=','');


                $collection = $oDiet->orderBy('meal_time','desc')->get();

                $items = $collection->map(function($item, $key) use ($who,$isCheckUser,$isRate){

                    $isCheck = true;
                    if ($isCheckUser) {
                        if ($who == 'nutritionist') {
                            if ($item->parent_member_id == 0 && $item->type == 'caloria') {
                                $isCheck = false;
                            }
                        }else if($who == 'myself'){
                            if ($item->read == 0 && $item->type == 'caloria' && $item->parent_member_id > 0) {
                                $isCheck = false;
                            }
                        }
                    }

                    return [
                        'rate'  => $isRate && $item->type == 'caloria' ? true : false,
                        'unique' => $item->id,
                        'photo' => storageURL($item->photo),
                        'check' => $isCheck
                    ];
                });


                if (count($items) > 0) {
                    $rows[] = [
                        "date" => $search_date,
                        "images" => $items
                    ];
                }
            }

        }else {
            $end_date = null;
        }

        $result['previous_date'] = isset($end_date) ?  Carbon::parse($end_date)->addDay(-1)->format("Y-m-d") : null;
        $result['data'] = $rows;


        return response()->json($result);
    }

}
