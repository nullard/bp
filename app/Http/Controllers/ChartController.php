<?php

namespace App\Http\Controllers;

use App\Diet;
use App\Member;
use App\MemberDrecord;
use App\Services\DietService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Kalnoy\Nestedset\Collection;

class ChartController extends Controller
{
    function __construct()
    {
    }

    function index() {

    }

    function week($member_unique,$date) {

//        $date = '2018-02-13';

        $period = getWeek($date);

        $oMember = Member::where('code',$member_unique)->first();

        $current_date = $period['sdate'];
        $rows = new Collection();
        while($current_date <= $period['edate']) {

            $rows[] = $this->getRecord($oMember,$current_date);

            $current_date = Carbon::parse($current_date)->addDay(1)->format('Y-m-d');
        }


        return response()->json($rows);
    }

    function month($member_unique,$date) {

//        $date = '2018-01-01';

        $oMember = Member::where('code',$member_unique)->first();

        $dt = Carbon::parse($date);

        $monthStart = $dt->copy()->startOfMonth();
        $monthEnd = $dt->copy()->endOfMonth();


        $current_date = Carbon::parse($monthStart)->format('Y-m-d');
        $last_date = Carbon::parse($monthEnd)->format('Y-m-d');

        while($current_date <= $last_date) {

            $rows[] = $this->getRecord($oMember,$current_date);

            $current_date = Carbon::parse($current_date)->addDay(1)->format('Y-m-d');
        }



        return response()->json($rows);

    }

    function sixMonth($member_unique,$date) {

        $oMember = Member::where('code',$member_unique)->first();

        $dt = Carbon::parse($date);


        if ($dt->format("m") <= 6) {
            $monthStart = $dt->format('Y').'-01-01';
            $monthEnd = Carbon::parse($monthStart)->addMonth(5)->endOfMonth()->format('Y-m-d');
        }else {
            $monthStart = $dt->format('Y').'-07-01';
            $monthEnd = Carbon::parse($monthStart)->addMonth(5)->endOfMonth()->format('Y-m-d');
        }

        $current_date = Carbon::parse($monthStart)->format('Y-m-d');
        $last_date = Carbon::parse($monthEnd)->format('Y-m-d');

        while($current_date <= $last_date) {

            $rows[] = $this->getRecord($oMember,$current_date);

            $current_date = Carbon::parse($current_date)->addDay(1)->format('Y-m-d');
        }



        return response()->json($rows);
    }

    function getRecord($oMember,$current_date) {

        $oRecord = MemberDrecord::with('weight_diet')->where('sdate',$current_date)
            ->where('member_id',$oMember->id)->first();


        $isMoon = 0;
        $targetValue = 0;
        if (!isset($oRecord) || $oRecord->target_value == 0) {
            $oTarget = MemberDrecord::where('member_id',$oMember->id)->where('sdate','<',$current_date)->where('target_cal','>',0)->orderBy('sdate','DESC')->first();
            if ($oTarget) {
                $targetValue = $oTarget->target_cal;
            }
        }

        $oStd = new \stdClass();
        $oStd->meal_time = $current_date;
        $oStd->value = 0;
        $oStd->sbp = null;
        $oStd->dbp = null;
        $oStd->standard_value = $targetValue;
        $oStd->isMoon = $isMoon;

        if ($oRecord) {
            $oStd->meal_time = $oRecord->sdate;
            $oStd->value = $oRecord->cal;
            $oStd->weight = $oRecord->weight > 0 ? $oRecord->weight : null;
            $oStd->sbp = $oRecord->sbp > 0 ? $oRecord->sbp : null;
            $oStd->dbp = $oRecord->dbp > 0 ? $oRecord->dbp : null;
            $oStd->isMoon = isset($oRecord->weight_diet) ? $oRecord->weight_diet->is_moon : 0;
        }


        return $oStd;
    }
}
