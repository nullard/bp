<?php

namespace App\Http\Controllers;

use App\Diet;
use App\Like;
use App\Member;
use App\Transforms\MemberTransform;
use Illuminate\Http\Request;
use Auth;
use App\Notifications\MyDynamic;

class LikeController extends Controller
{
    public function index()
    {

    }

    public function show($unique)
    {

        $likeList = Like::select('member_id')->where('diet_id',$unique)->distinct()->get();

        $members = $likeList->pluck('member_id')->toArray();
        $collecion = Member::where(function($query) use($members){
            $query->whereIn('id',$members)->get();
        })->get();//->limit(20)

        $rows['data'] = (new MemberTransform())->transform($collecion);

        return response()->json($rows);

    }

    public function save(Request $request)
    {
        $user = Auth::guard('front')->user();

        $row = Like::where('member_id',$user->id)->where('diet_id',$request->diet_id)->first();

        $toMember = Member::find(Diet::find($request->diet_id)->member_id);

        if ($row){
            $row->delete();
            $toMember->notify(new MyDynamic($user,'unlike', $request->diet_id));
        }else {
            Like::create([
                'member_id'  => $user->id,
                'diet_id'    => $request->diet_id
            ]);
            $toMember->notify(new MyDynamic($user,'like', $request->diet_id));
        }


    }
}
