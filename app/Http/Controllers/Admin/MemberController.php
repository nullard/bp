<?php

namespace App\Http\Controllers\Admin;

use App\MemberDrecord;
use App\Track;
use App\Transforms\MemberTransform;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use App\User;
use App\OrderPayment;
use App\MemberKgTarget;
use Auth;
use Illuminate\Support\Facades\Input;

class MemberController extends Controller
{
    public function index($sdate=null)
    {
        $rows = Member::orderBy('id','desc');

        $appends = [];
        if (Input::has('name')) {
            $appends['name'] = Input::get('name');
            $name = $appends['name'];
            $rows->where(function($query) use($name){
                $query->where('name','like',"%$name%")
                    ->orWhere('real_name','like',"%$name%");
            });
        }
        if (Input::has('pay_status')) {
            $appends['pay_status'] = Input::get('pay_status');
            $now = Carbon::now()->format('Y-m-d');

            if ($appends['pay_status'] == 1) {
                $rows->whereIn('id',function($query) use($now){

                    $query->select('member_id')
                        ->from(with(new OrderPayment())->getTable())
                        ->where('sdate', '<=', $now)
                        ->where('edate','>=',$now)
                        ->where('status',1);
                });
            }else {
                $rows->whereNotIn('id',function ($query) use($now){

                    $query->select('member_id')
                        ->from(with(new OrderPayment())->getTable())
                        ->where('sdate', '<=', $now)
                        ->where('edate','>=',$now)
                        ->where('status',1);

                });
            }

        }

        if (Input::has('user_id')) {
            $appends['user_id'] = Input::get('user_id');
            $rows->where('user_id',$appends['user_id']);
        }

        if (Input::has('recommand_id')) {
            $appends['recommand_id'] = Input::get('recommand_id');
            $rows->where('recommand_id',$appends['recommand_id']);
        }


        if(Auth::user()->role == 'nutritionist') {
            $rows->where('parent_id',Auth::id());
        }

        $rows = $rows->paginate(20);

        $data = $rows->map(function($row) {
            return (new MemberTransform)->transformRow($row);
        });//(new MemberTransformer)->transform($rows->items());

        return view('admin.members.list',compact('rows','sdate','data'))
            ->with([
                'appends' => $appends,
                'nutritionists' => Member::where('level','nutritionist')->get(),
            ]);
    }

    function show($id) {

        $row = Member::with('latestKgTarget')->find($id);
        $row = (new MemberTransform)->transformRow($row);

        $payments = OrderPayment::where('member_id',$row->id)->orderBy('id','dsc')->get();

        return view('admin.members.show',compact('row','payments'));
    }

    function edit($id) {

        $row = Member::with('latestKgTarget')->find($id);
        $row = (new MemberTransform)->transformRow($row);

        return view('admin.members.edit',compact('row'));
    }

    function store(Request $request) {

        $row = Member::find($request->id);


        $row->parent_id = $request->parent_id;
        $row->real_name = $request->real_name;
        $row->level = $request->level;
        $row->sex = $request->sex;
        $row->birth = $request->birth;
        $row->pay = $request->pay;
        $row->status = isset($request->status) ? $request->status : 0;
        $row->vegetarian = isset($request->vegetarian) ? $request->vegetarian : 0;
        $row->save();

       if ($request->level == 'patient' && $request->parent_id > 0) {

           $oTrack = Track::where('member_id',$row->id)->where('track_member_id',$request->parent_id)->first();

           if (!$oTrack) {
               Track::create([
                   "member_id" => $row->id,
                   "track_member_id" => $request->parent_id,
                   "approve" => 1
               ]);
           }else {
               if ($oTrack->approve == 0) {
                   $oTrack->approve = 1;
                   $oTrack->save();
               }
           }

           $oTrackreverse = Track::where('member_id',$request->parent_id)->where('track_member_id',$row->id)->first();

           if (!$oTrackreverse) {
               Track::create([
                   "member_id" => $request->parent_id,
                   "track_member_id" => $row->id,
                   "approve" => 1
               ]);
           }else {
               if ( $oTrackreverse->approve == 0) {
                   $oTrackreverse->approve = 1;
                   $oTrackreverse->save();
               }
           }

       }

        $oKgTarget = MemberKgTarget::find($request->member_kg_target_id) ?: new MemberKgTarget;
        $oKgTarget->member_id = $row->id;
        $oKgTarget->tall = $request->tall;
        $oKgTarget->weight = $request->weight;
        $oKgTarget->save();

        return redirect(route('admin.members'));

    }

    public function destroy(Member $member)
    {
        if ($member) {
            $member->social_id = "[D]".$member->social_id;
            $member->status = 0;
            $member->deleted_at = Carbon::now();
            $member->save();
        }

        return redirect('admin/member');
    }
}
