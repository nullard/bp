<?php

namespace App\Http\Controllers\Admin;

use App\Recommand;
use App\RecommandResult;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecommandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {

        $rows = Recommand::paginate(20);

        return view('admin.recommands.list',compact('rows'));

    }

    public function edit($id = null)
    {

        if (isset($id)) {

            $row = Recommand::find($id);

        }else {

            $row = new Recommand();

        }

        return view('admin.recommands.edit',compact('row'));
    }

    public function show($id)
    {

        $row = Recommand::find($id);
        $rowResults = RecommandResult::where('recommand_id',$row->id)->get();

        return view('admin.recommands.show',compact('row','rowResults'));
    }

    public function store(Request $request)
    {

        if (empty($request->id)) {
            $o = new Recommand();
//            $o->qrcode = Carbon::now()->timestamp;
        }else {
            $o = Recommand::find($request->id);
        }

        $o->qrcode = empty($request->qrcode) ? Carbon::now()->timestamp : $request->qrcode;
        $o->type = $request->type;
        $o->name = $request->name;
        $o->member_id = $request->member_id;
        $o->status = isset($request->status) ? $request->status : 0;

        $o->save();




        return redirect(route('admin.recommand'));
    }
}
