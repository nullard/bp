<?php

namespace App\Http\Controllers;

use App\MemberDrecord;
use App\Services\DietService;
use App\Diet;
use App\MemberKgTarget;
use App\Services\OtherDynamicService;
use App\Track;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Services\So88Upload;
use App\Services\UploadService;
use App\Notifications\MyDynamic;
use Auth;
use Illuminate\Support\Facades\Artisan;
use Intervention\Image\Facades\Image;

class DietController extends Controller
{
    public function __construct(DietService $dietService)
    {
        $this->dietService = $dietService;
    }

    public function show($unique)
    {
        $o = Diet::with('member')->find($unique);

        if ($o) {

            $isView = false;
            $proportion = true;
            $user = Auth::guard('front')->user();

            /*
            if ($o->share_type == "public") {
                $isView = true;
            }else if($user){

                if($user->id == $o->member_id) {
                    $isView = true;
                }else {
                    switch($o->share_type) {
                        case "tracker":
                            $count = Track::where('member_id',$user->id)
                                ->where('track_member_id',$o->member_id)
                                ->where('approve',1)
                                ->count();

                            if ($count > 0 ) {
                                $isView = true;
                            }

                            break;
                        case "expert":
                            if ($user->id == $o->member->parent_id) {
                                $isView = true;
                            }
                            break;
                        default:

                            break;
                    }
                }
            }*/
            $count = Diet::dietShare($user)->where('id',$o->id)->count();

            if ($count > 0) {
                $isView = true;
            }


            if ($isView) {
                if ($user) {
                    if ($user->level == 'nutritionist' && $user->id == $o->member->parent_id) {
                        $proportion = false;
                    }
                }

                $row = $this->dietService->transformData($o,$proportion);
            }else {
                $row = ['status' => '403'];
            }

        }else {
            $row = ['status' => '404'];
        }

        return response()->json($row);
    }

    public function save(Request $request){

        $user = Auth::guard('front')->user();

        if ($user) {

            $oDiet = new Diet();

            $isEdit = false;
            if (!empty($request->unique)) {
                $oDiet = Diet::find($request->unique);
                if ($oDiet && $oDiet->member_id == $user->id) {
                    $isEdit = true;
                }else {
                    $oDiet = new Diet();
                }
            }

            $oDiet->type = $request->type;
            switch($oDiet->type) {
                case 'caloria':
                    $oDiet->meal_time = $request->meal_time;
                    $oDiet->name = $request->name;
                    break;
                case 'bp':
                    $oDiet->meal_time = $request->meal_time;
                    $oDiet->value = doubleval($request->value);
                    $oDiet->sbp = doubleval($request->sbp);
                    $oDiet->dbp = doubleval($request->dbp);

                    $oDiet->is_moon = $request->isMoon == "true" ? 1 : 0;


                    break;
                default:
                    $oDiet->meal_time = Carbon::now()->format('Y-m-d H:i:s');
                    break;

            }
            $oDiet->desp = $request->desp;
            $oDiet->share_type = $request->share_type;
            $oDiet->member_id = $user->id;

            if ($request->share_type == 'public') {
                if ( $user->level != 'nutritionist' ) {
                    $oDiet->share_type = "expert";
                }
            }


            if (isset($request->photo)){
                $data = explode( ',', $request->photo );

                if (preg_match("/^(\/uimages|\/uprofiles)/i", $oDiet->photo)) {

                    preg_match("/^(\/uimages|\/uprofiles)(.*)/i", $oDiet->photo, $matches, PREG_OFFSET_CAPTURE);

                    $file_path = "/images".$matches[2][0];
                }else {
                    $file_path = $oDiet->photo;
                }

                if (base64_encode(file_get_contents(So88Upload::rootPath().$file_path)) == $data[1]) {
                    // nothing
                }else {
                    if ($isEdit) {
                        @unlink(So88Upload::rootPath().$file_path);
                    }

                    $relativePath = '/images/'.$user->id.'/';
                    $filename = (new UploadService)->fromBase64($request->photo,So88Upload::rootPath().$relativePath,1024);

                    $oDiet->photo = $relativePath.$filename;
                }
            }else {

                if (!$isEdit) {
                    $oDiet->photo = '';
                }

            }

            $oDiet->save();


            if ($oDiet->type == 'caloria' && $user->parent_id == 0) {
                if ( isset($oDiet->name) ) {
                    Artisan::queue('diet:caloria', [
                        'food' => $oDiet->name,
                        'diet_id' => $oDiet->id,
                        'notification' => $isEdit ? 0 : 1
                    ])->delay(30);
                }
            }


            if (!$isEdit) {
                (new OtherDynamicService())->notification($user,$oDiet,$isEdit ? 1 : 0);
            }

            return json_encode([
                "code" => 200,
                "data" => [
                    "image_url" => !$isEdit && !empty($oDiet->photo) ? config('app.url').storageURL($oDiet->photo) : '',
                    "redirect_url" => config('app.url')
                ]
            ]);

        }else {
            echo 'User Not Check';
        }

    }

    public function rate(Request $request,$unique)
    {

        $user = Auth::guard('front')->user();

        $oDiet = Diet::with('member')->findOrFail($unique);

        if ($this->isOwnNutritionist($user,$oDiet->member->parent_id)) {

            $toMember = $oDiet->member;
            $toMember->notify(new MyDynamic($user,'rated',$oDiet->id,$oDiet->parent_member_id > 0 ? 1 : 0));


            $oDiet->protein = $request->protein;
            $oDiet->fat = $request->fat;
            $oDiet->sugar = $request->sugar;
            $oDiet->parent_member_id = $user->id;
            $oDiet->value = getCaloria($oDiet->sugar,$oDiet->protein,$oDiet->fat);

            $oDiet->remark = $request->remark;

            $oDiet->save();


            //handle
           $user->unreadNotifications->where('type','App\Notifications\OtherDynamic')
                ->where('data.type',"caloria")
                ->where('data.relative_id',$oDiet->id)
                ->markAsRead();
        }


        return response()->json([]);
    }

    public function setPrivacy(Request $request) {
        $user = Auth::guard('front')->user();


        $oDiet = Diet::where('id',$request->unique)->first();

        if ($oDiet) {

            if ($user->id == $oDiet->member_id) {

                $oDiet->share_type = $request->share_type;
                $oDiet->save();

            }

        }



    }

    public function read(Request $request,$unique)
    {
        $user = Auth::guard('front')->user();

        $oDiet = Diet::with('member')->findOrFail($unique);

        if ($this->isOwn($user,$oDiet->member->id)) {
            $oDiet->read = 1;
            $oDiet->save();
        }

        return response()->json([]);
    }

    public function delete(Request $request)
    {
        $user = Auth::guard('front')->user();
        $oDiet = Diet::find($request->unique);

        if ($oDiet->member_id == $user->id) {
            $oDiet->delete();
        }

    }

    private function isOwnNutritionist($user,$parent_id) {

        return $user->level == 'nutritionist' && $parent_id == $user->id;

    }

    private function isOwn($user,$member_id) {
        return $user->id == $member_id;
    }
}
