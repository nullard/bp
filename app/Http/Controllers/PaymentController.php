<?php

namespace App\Http\Controllers;

use App\Member;
use App\Notifications\MemberLineNotification;
use App\Odrnos;
use App\Order;
use App\OrderPayment;
use App\PaymentLog;
use App\RecommandResult;
use App\RecommandResultItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;

abstract class So88Product {
    const Amount = 1000;
    const Name = '瘦88瘦身課程1年期';

    const Period = ['type'=>'M','frequency'=>1,'times'=>12];
//    const Period = ['type'=>'D','frequency'=>1,'times'=>3];


    static function products($total,$invoice = 0) {

        if ($invoice == 1) {
            $a = [
                'ItemName' => self::Name,
                'ItemCount' => 1,
                'ItemWord' => 'Unit',
                'ItemPrice' => $total,
                'ItemTaxType' => \ECPay_TaxType::Dutiable,
                'ItemAmount' => $total * 1];
        }else {
            $a = [
                'Name' => self::Name,
                'Price' => (int)$total,
                'Currency' => "元",
                'Quantity' => (int) "1",
                'URL' => ""];
        }



        return $a;
    }
}

class PaymentController extends Controller
{
    use Notifiable;
    var $debug = true;
    var $oEcpay;
    var $oInvoice;

    function __construct()
    {
        if (config('app.env') == 'production') {
            $this->debug = false;
        }
    }

    function instance($instanceClass=null) {

        if (\ECPay_AllInOne::class == $instanceClass) {
            try {
                $this->oEcpay = new \ECPay_AllInOne();
                $this->oEcpay->ServiceURL  = $this->debug ? config('services.ecpay.dev.service_url') : config('services.ecpay.release.service_url');
                $this->oEcpay->HashKey     = $this->debug ? config('services.ecpay.dev.hashkey') : config('services.ecpay.release.hashkey');
                $this->oEcpay->HashIV      = $this->debug ? config('services.ecpay.dev.hashiv') : config('services.ecpay.release.hashiv');
                $this->oEcpay->MerchantID  = $this->debug ? config('services.ecpay.dev.merchantid') : config('services.ecpay.release.merchantid');
                $this->oEcpay->EncryptType = '1';

            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        if (\EcpayInvoice::class == $instanceClass) {
            try {
                $this->oInvoice = new \EcpayInvoice();
                $this->oInvoice->Invoice_Url = $this->debug ? config('services.ecpay.dev.invoice_service_url') : config('services.ecpay.release.invoice_service_url');
                $this->oInvoice->HashKey = $this->debug ? config('services.ecpay.dev.invoice_hashkey') : config('services.ecpay.release.invoice_hashkey');
                $this->oInvoice->HashIV = $this->debug ? config('services.ecpay.dev.invoice_hashiv') : config('services.ecpay.release.invoice_hashiv');
                $this->oInvoice->MerchantID  = $this->debug ? config('services.ecpay.dev.merchantid') : config('services.ecpay.release.merchantid');
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

    }

    function payment(Request $request) {


        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
        ]);

        $this->instance(\ECPay_AllInOne::class);



        $odr_no = (new Odrnos())->generateNO('SO',Carbon::now()->format('ymd'));
        $oMember = (new Member())->getDataByToken($request->token);
        $oOrder = new Order();

        $TotalAmount  = empty($oMember->pay) ? So88Product::Amount : $oMember->pay;

        $oOrder->odr_no = $odr_no;
        $oOrder->name = $request->name;
        $oOrder->email = $request->email;
        $oOrder->member_id = $oMember->id;
        $oOrder->total = $TotalAmount;
        $oOrder->trade_desp = So88Product::Name;
        $oOrder->third_party = 'ecpay';
        $oOrder->save();

        $oMember->real_name = empty($oMember->real_name) ? $request->name : $oMember->real_name;
        $oMember->email = empty($oMember->email) ? $request->email : $oMember->email;
        $oMember->pay = $TotalAmount;
        $oMember->save();

        $MerchantTradeNo = $odr_no;
        $TradeDesc = So88Product::Name;

        //基本參數(請依系統規劃自行調整)
        $this->oEcpay->Send['ReturnURL']         = url('/api/payment/callback');
        $this->oEcpay->Send['MerchantTradeNo']   = $MerchantTradeNo;
        $this->oEcpay->Send['MerchantTradeDate'] = Carbon::now()->format('Y/m/d H:i:s');
        $this->oEcpay->Send['TotalAmount']       = $TotalAmount;
        $this->oEcpay->Send['TradeDesc']         = $TradeDesc;
        $this->oEcpay->Send['ChoosePayment']     = \ECPay_PaymentMethod::Credit;

        //訂單的商品資料
        array_push($this->oEcpay->Send['Items'], So88Product::products($TotalAmount));


        //Credit信用卡定期定額付款延伸參數(可依系統需求選擇是否代入)
        $this->oEcpay->SendExtend['PeriodAmount'] = $TotalAmount ;    //每次授權金額，預設空字串
        $this->oEcpay->SendExtend['PeriodType']   = So88Product::Period['type'] ;    //週期種類，預設空字串
        $this->oEcpay->SendExtend['Frequency']    = So88Product::Period['frequency'] ;    //執行頻率，預設空字串
        $this->oEcpay->SendExtend['ExecTimes']    = So88Product::Period['times'] ;    //執行次數，預設空字串
        $this->oEcpay->SendExtend['PeriodReturnURL'] = url('/api/payment/period/callback');

        //產生訂單(auto submit至ECPay)
        $this->oEcpay->CheckOut();
    }

    function callback() {

        try
        {
            $this->instance(\ECPay_AllInOne::class);

            $isPayOK = false;
            $arFeedback = $this->oEcpay->CheckOutFeedback();

            Log::info('order_create => '.json_encode($arFeedback));
            if (sizeof($arFeedback) > 0) {
                foreach ($arFeedback as $key => $value) {
                    switch ($key)
                    {
                        /* 支付後的回傳的基本參數 */
                        case "MerchantID": $szMerchantID = $value; break;
                        case "MerchantTradeNo": $szMerchantTradeNo = $value; break;
                        case "PaymentDate": $szPaymentDate = $value; break;
                        case "PaymentType": $szPaymentType = $value; break;
                        case "PaymentTypeChargeFee": $szPaymentTypeChargeFee = $value; break;
                        case "RtnCode": $szRtnCode = $value; break;
                        case "RtnMsg": $szRtnMsg = $value; break;
                        case "SimulatePaid": $szSimulatePaid = $value; break;
                        case "TradeAmt": $szTradeAmt = $value; break;
                        case "TradeDate": $szTradeDate = $value; break;
                        case "TradeNo": $szTradeNo = $value; break;
                        case "PayAmt": $szPayAmt = $value; break;
                        case "RedeemAmt": $szRedeemAmt = $value; break;
                        default: break;
                    }
                }

                $merchantID = $this->debug ? config('services.ecpay.dev.merchantid') : config('services.ecpay.release.merchantid');


                $oOrder = Order::where('odr_no',$szMerchantTradeNo)->first();
                $oMember = Member::find($oOrder->member_id);

                $this->setLog('order_create',$oOrder,$arFeedback,$szSimulatePaid);

                $oOrder->third_party_no = $szTradeNo;
                $oOrder->save();

                $parse = $szPaymentDate;
                $parse_result = Carbon::parse($parse)->format('Y-m-d');

                if (Carbon::parse($parse)->endOfMonth()->format('Y-m-d') == $parse_result) {
                    $edate = Carbon::parse($parse)->addDay(1)->endOfMonth()->addDay(-1)->format('Y-m-d');
                }else {
                    $edate = Carbon::parse($parse)->addMonth()->addDay(0)->format('Y-m-d');
                }

                $oOrderPayment = OrderPayment::updateOrCreate(
                    ['order_id'=>$oOrder->id,'member_id'=>$oOrder->member_id,'pay_date'=>$parse_result,'sdate'=>$parse_result,'edate'=>$edate],
                    ['total' => $oOrder->total]
                );

                if ($szRtnCode == 1 && $szMerchantID == $merchantID ) {
                    $isPayOK = true;

                    if ($szSimulatePaid == 0 && $oOrderPayment->status != 1) {

                        $oOrderPayment->status = 1;
                        $oOrderPayment->save();

                        if (OrderPayment::where('order_id',$oOrder->id)->count() == 1) {
                            $oMember->pay_sdate = $parse_result;
                            $oMember->pay_edate = Carbon::parse($parse_result)->addYear(1)->format('Y-m-d');
                            $oMember->save();
                        }

                        if ($oOrder->total_success_times == 0) {
                            $oOrder->total_success_times = 1;
                            $oOrder->save();
                        }

                        $this->createInvoice($oOrder,$oOrderPayment);
                    }
                }else {


                    $preload = ['oOrderPayment'=>$oOrderPayment];
                    $this->notify(new MemberLineNotification('PAY_FAIL','text',$oMember->social_id,$preload));

                    exit;
                }

            }

            if ($isPayOK) {
                $response = '1|OK';
            }else {
                $response = '0|Fail';
            }

        } catch (Exception $e) {
            $error = '0|' . $e->getMessage();

            $this->setLog('order_create_fail','0',$error,$szSimulatePaid);
        }


        if (ob_get_contents()) ob_end_clean();

        return $response;
    }

    public function periodCallback()
    {
        try
        {
            $this->instance(\ECPay_AllInOne::class);

            $isPayOK = false;
            $arFeedback = $this->oEcpay->CheckOutFeedback();

            Log::info('order_create => '.json_encode($arFeedback));
            if (sizeof($arFeedback) > 0) {
                foreach ($arFeedback as $key => $value) {
                    switch ($key)
                    {
                        /* 支付後的回傳的基本參數 */
                        case "MerchantID": $szMerchantID = $value; break;
                        case "MerchantTradeNo": $szMerchantTradeNo = $value; break;
                        case "RtnCode": $szRtnCode = $value; break;
                        case "SimulatePaid": $szSimulatePaid = $value; break;
                        case "ProcessDate": $szProcessDate = $value; break;
                        case "TotalSuccessTimes": $szTotalSuccessTimes = $value; break;
                        default: break;
                    }
                }

                $merchantID = $this->debug ? config('services.ecpay.dev.merchantid') : config('services.ecpay.release.merchantid');

                $oOrder = Order::where('odr_no',$szMerchantTradeNo)->first();
                $oMember = Member::find($oOrder->member_id);

                $this->setLog('order_period',$oOrder,$arFeedback,$szSimulatePaid);


                $parse = $szProcessDate;
                $parse_result = Carbon::parse($parse)->format('Y-m-d');

                if (Carbon::parse($parse)->endOfMonth()->format('Y-m-d') == $parse_result) {
                    $edate = Carbon::parse($parse)->addDay(1)->endOfMonth()->addDay(-1)->format('Y-m-d');
                }else {//previous addDay(-1)
                    $edate = Carbon::parse($parse)->addMonth()->addDay(0)->format('Y-m-d');
                }

                $oOrderPayment = OrderPayment::updateOrCreate(
                    ['order_id'=>$oOrder->id,'member_id'=>$oOrder->member_id,'pay_date'=>$parse_result,'sdate'=>$parse_result,'edate'=>$edate],
                    ['total' => $oOrder->total]
                );

                if ($szRtnCode == 1 && $szMerchantID == $merchantID ) {
                    $isPayOK = true;


                    if ($szSimulatePaid == 0 && $oOrderPayment->status != 1) {

                        $oOrderPayment->status = 1;
                        $oOrderPayment->save();

                        if (OrderPayment::where('order_id',$oOrder->id)->count() == 1) {
                            $oMember->pay_sdate = $parse_result;
                            $oMember->pay_edate = Carbon::parse($parse_result)->addYear(1)->format('Y-m-d');
                            $oMember->save();
                        }

                        if ($oOrder->total_success_times <  $szTotalSuccessTimes) {
                            $oOrder->total_success_times = $szTotalSuccessTimes;
                            $oOrder->save();
                        }

                        $this->createInvoice($oOrder,$oOrderPayment);
                    }
                }else {


                    $preload = ['oOrderPayment'=>$oOrderPayment];
                    $this->notify(new MemberLineNotification('PAY_FAIL','text',$oMember->social_id,$preload));

                    exit;
                }

            }

            if ($isPayOK) {
                $response = '1|OK';
            }else {
                $response = '0|Fail';
            }

        } catch (Exception $e) {
            $error = '0|' . $e->getMessage();

            $this->setLog('order_create_fail','0',$error,$szSimulatePaid);
        }


        if (ob_get_contents()) ob_end_clean();

        return $response;

    }

    function createInvoice($order,$oOrderPayment) {


        if (empty($oOrderPayment->invoice_number)) {

            $oMember = Member::with('latestKgTarget')->find($order->member_id);

            $oRememberResult = RecommandResult::updateOrCreate(
                ['recommand_id'=>$oMember->recommand_id,'recommand_date'=>date('Y-m-d')]
            );
            $oRememberResult->member_pay_count = $oRememberResult->member_pay_count + 1;
            $oRememberResult->save();

            $oRecommandItem = new RecommandResultItem();
            $oRecommandItem->recommand_result_id = $oRememberResult->id;
            $oRecommandItem->type = 'add_member';
            $oRecommandItem->fromtable_id = $oOrderPayment->id;
            $oRecommandItem->fromtable_type = 'order_payments';
            $oRecommandItem->save();

            $this->instance(\EcpayInvoice::class);

            $this->oInvoice->Invoice_Method = \EcpayInvoiceMethod::INVOICE;

            array_push($this->oInvoice->Send['Items'], So88Product::products($order->total,1));

            $this->oInvoice->Send['RelateNumber'] = $order->odr_no.'-'.$oOrderPayment->id;
            $this->oInvoice->Send['CustomerName'] = $order->name;
            $this->oInvoice->Send['CustomerEmail'] = $order->email;
            $this->oInvoice->Send['Print'] = \ECPay_PrintMark::No;
            $this->oInvoice->Send['Donation'] = \ECPay_Donation::No;
            $this->oInvoice->Send['TaxType'] = \ECPay_TaxType::Dutiable;
            $this->oInvoice->Send['SalesAmount'] = $order->total;
            $this->oInvoice->Send['InvoiceRemark'] = '';
            $this->oInvoice->Send['InvType'] = \ECPay_InvType::General;

            $aReturn_Info = $this->oInvoice->Check_Out();

            $this->setLog('invoice_create', $order, $aReturn_Info, 0);

            foreach ($aReturn_Info as $key => $value) {
                switch ($key) {
                    case "RtnCode":
                        $iv_RtnCode = $value;
                        break;
                    case "RtnMsg":
                        $iv_RtnMsg = $value;
                        break;
                    case "InvoiceNumber":
                        $iv_InvoiceNumber = $value;
                        break;
                    case "InvoiceDate":
                        $iv_InvoiceDate = $value;
                        break;
                    case "RandomNumber":
                        $iv_RandomNumber = $value;
                        break;
                    case "CheckMacValue":
                        $iv_CheckMacValue = $value;
                        break;
                    default:
                        break;
                }
            }

            $oOrderPayment->invoice_number = $iv_InvoiceNumber;
            $oOrderPayment->invoice_date = $iv_InvoiceDate;
            $oOrderPayment->invoice_rand_number = $iv_RandomNumber;
            $oOrderPayment->save();



            $preload = ['oOrderPayment'=>$oOrderPayment];
            $this->notify(new MemberLineNotification('PAY_FINISHED','text',$oMember->social_id,$preload));


            $preload = ['oMember' => $oMember];
            $isNotify = false;

            if (count($oMember->latestKgTarget) == 0) {
                $isNotify = true;
            }else {
                if ($oMember->latestKgTarget[0]->weight == 0.0) {
                    $isNotify = true;
                }
            }
            if ($isNotify) {
                $this->notify(new MemberLineNotification('FILLED_INFO','buttons',$oMember->social_id,$preload));
            }

        }
    }

    function setLog($type,$order,$desp,$simulate=0) {

        $o = new PaymentLog();
        $o->type = $type;
        $o->third_party = 'ecpay';
        $o->order_id = $order->id;
        $o->desp = json_encode($desp);
        $o->simulate = $simulate;
        $o->save();

    }


}
