<?php

namespace App\Http\Controllers;

use App\Diet;
use App\Services\NutriProportion;
use Illuminate\Http\Request;
use App\Member;
use App\MemberDrecord;
use Carbon\Carbon;
use DB;
use Auth;

class MemberDrecordController extends Controller
{
    function index($member_unique) {

        $oMember = Member::where('code',$member_unique)->first();

        if ($oMember) {
            $oRecord = MemberDrecord::getRecord($oMember->id);

            $tall = 0;
            if (isset($oMember->latestKgTarget[0])) {
                $tall = $oMember->latestKgTarget[0]['tall'];
            }

            $proportion = Diet::select(DB::raw('SUM(protein) as sum_protein, SUM(fat) as sum_fat, SUM(sugar) as sum_sugar '))
                ->where('type','caloria')
                ->where('member_id',$oMember->id)
                ->where('parent_member_id','>',0)
                ->whereDate('created_at', '>=', Carbon::now()->addDay(-60)->format('Y-m-d H:i:s'))
                ->first();

            $proportion_weight = $proportion->sum_protein * NutriProportion::$protein + $proportion->sum_fat * NutriProportion::$fat + $proportion->sum_sugar * NutriProportion::$sugar;
            $percentage = $proportion_weight == 0 ? 0 : $proportion->sum_sugar * NutriProportion::$sugar / $proportion_weight;
            $addict = round( $percentage * 100 , 2);

            $weight = $oRecord->weight;
            if ($oRecord->weight == 0) {
                $oRecentWeight = MemberDrecord::where('member_id',$oMember->id)
                    ->where('weight','>', 0)
                    ->latest()
                    ->first();
                if ($oRecentWeight) {
                    $weight = $oRecentWeight->weight;
                }
            }


            return response()->json(
                [
                    'sdate'         => $oRecord->sdate,
                    'target_cal'    => $oRecord->target_cal,
                    'suggestions'   => $oRecord->suggestions,
                    'tasks'         => $oRecord->tasks,
                    'bmi'           => $tall > 0 ? round($weight / pow($tall / 100,2),1) : 0,
                    'addict'        => $addict
                ]
            );
        }

    }

    function save(Request $request) {

        if (Auth::guard('front')->user()->level == 'nutritionist') {

            $oMember = Member::where('code',$request->unique)->first();

            if ($oMember && $oMember->parent_id == Auth::guard('front')->user()->id) {

                $oRecord = MemberDrecord::getRecord($oMember->id);

                $oRecord->target_cal = $request->target_cal ? $request->target_cal : $oRecord->target_cal;
                $oRecord->suggestions = $request->suggestions ? $request->suggestions : $oRecord->suggestions;
                $oRecord->tasks = $request->tasks ? $request->tasks : $oRecord->tasks;

                $oRecord->save();

            }
        }

    }

}
