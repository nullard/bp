<?php

namespace App\Http\Controllers;

use App\Member;
use App\Comment;
use App\Diet;
use App\Notifications\MyDynamic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class CommentController extends Controller
{
    public function index()
    {

        $user = Auth::guard('front')->user();

        $user->notify(new MyDynamic($user,'track'));

        //$rs = $user->unreadNotifications->where("data.id",1);
        //dd($rs);
//        dd($user->unreadNotifications->markAsRead());
    }

    public function delete(Request $request)
    {

        $user = Auth::guard('front')->user();
        $type = $request->type;

        if ($type == "comment") {
            $comment = Comment::find($request->unique);
            if ($user->id == $comment->member_id) {
                $comment->delete();
            }
        }else if($type == "diet"){
            $diet = Diet::find($request->unique);
            if ($user->id == $diet->member_id) {
                $diet->desp = "";
                $diet->update();
            }
        }else if($type == "diet_remark") {
            $diet = Diet::find($request->unique);
            if ($user->id == $diet->parent_member_id) {
                $diet->remark = "";
                $diet->update();
            }
        }
    }

    public function save(Request $request)
    {
        $user = Auth::guard('front')->user();

        if (isset($request->unique)) {
            $type = $request->type;

            if ($type == "comment") {
                $model = Comment::find($request->unique);
                if ($user->id == $model->member_id) {
                    $model->message = $request->message;
                    $model->update();
                }
            }else if($type == "diet"){
                $model = Diet::find($request->unique);
                if ($user->id == $model->member_id) {
                    $model->desp = $request->message;
                    $model->update();
                }
            }else if ($type == 'diet_remark') {
                $model = Diet::find($request->unique);
                if ($user->id == $model->parent_member_id) {
                    $model->remark = $request->message;
                    $model->update();
                }
            }

        }else {

            $type = "comment";
            $model = Comment::create([
                'member_id' => $user->id,
                'message'   => $request->message,
                'diet_id'   => $request->diet_id
            ]);

            $toMember = Member::find(Diet::find($request->diet_id)->member_id);
            if ($toMember->id != $user->id) {
                $toMember->notify(new MyDynamic($user,'comment',$request->diet_id));
            }


        }

        return response()->json(["data"=> [
            "type"  => $type,
            "unique" => $model->id
        ]]);

    }
}
