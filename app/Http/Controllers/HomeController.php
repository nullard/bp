<?php

namespace App\Http\Controllers;

use App\Member;
use App\Services\DietService;
use App\Track;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Diet;
use App\Services\NutriProportion;
use Illuminate\Support\Facades\Input;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DietService $dietService)
    {
        $this->dietService = $dietService;
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type=null,$user_unique=null)
    {
        $date = Input::get('search_date');
        if (!isset($date)) {
            $date = Carbon::now()->format('Y-m-d H:i:s');
        }

        // share type
        $user = Auth::guard('front')->user();

        $oDiet = Diet::dietShare($user)
            ->with(['member','user'])
            ->whereHas('member',function($query) {
                $query->where('status',1);
            })
            ->where('created_at','<=', $date)->whereNotNull('type');

        if ($type == 'track') {
            if ($user_unique) {
                $user = Member::where('code',$user_unique)->first();
            }

            if ($user) {
                $trackArray = Track::trackListArray($user->id);
                $oDiet->whereIn('member_id',$trackArray);
            }
        }


        $collection = $oDiet->latest()->paginate(25);

        $collection->getCollection()->transform(function($item, $key) {

            $row = $this->dietService->transformData($item);

            return $row;
        });


        $collection->appends('search_date',$date);


        return response()->json($collection);
    }



}
