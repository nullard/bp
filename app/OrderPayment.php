<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $fillable = [
        'order_id','member_id','pay_date','sdate','edate','total','status','invoice_number','invoice_date','invoice_rand_number'
    ];

    public static function isPay($member_id,$date=null) {

        $check_date = Carbon::now()->format('Y-m-d');
        if (isset($date)) {
            $check_date = $date;
        }

        $count = self::where('member_id',$member_id)
            ->where('status',1)
            ->where('sdate','<=',$check_date)
            ->where('edate','>=',$check_date)->count();

        if ($count == 0) {
            return false;
        }

        return true;

    }

    public static function recentPaymentInfo($member_id) {

        $check_date = Carbon::now()->format('Y-m-d');

        $row = self::where('member_id',$member_id)
            ->where('status',1)
            ->where('sdate','<=',$check_date)
            ->where('edate','>=',$check_date)->latest()->first();

        return $row;

    }
}
