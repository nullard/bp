<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/2/21
 * Time: 下午4:55
 */

namespace App\Transforms;


use App\Recommand;
use App\Track;
use Illuminate\Support\Collection;
use Auth;
use App\Member;

class MemberTransform
{

    public function transform(Collection $collection)
    {

        $user = Auth::guard('front')->user();


        $result = $collection->map(function($item) use($user){

            $track = -99;

            if ($user) {

                $row = Track::where('member_id',$user->id)->where('track_member_id',$item->id)->first();

                if ($row) {
                    $track = $row->approve;
                }

            }

            return [
                'user_unique' => $item->code,
                'user_role' => $item->level,
                'user_name'=> $item->name,
                'user_img' => storageURL($item->pic),
                'track'=> $track
            ];
        });



        return $result;
    }

    public function transformRow(Member $member)
    {
        $info = $member->latestKgTarget;

        $tall = 0;
        $weight = 0;
        if (isset($info)) {
            if (isset($info[0])) {
                $tall = $info[0]['tall'];
                $weight = $info[0]['weight'];
            }
        }

        if ( $member->parent_id > 0 ) {
            $parent = Member::find($member->parent_id);
        }

        return (Object)[
            'id' => $member->id,
            'tall' => $tall,
            'weight' => $weight,
            'real_name' => $member->real_name,
            'name'  => $member->name,
            'sex'   => $member->sex,
            'status'   => $member->status,
            'level' =>  $member->level,
            'birth' => $member->birth,
            'parent_id' => $member->parent_id,
            'pay' => $member->pay,
            'type' => $member->type,
            'social_id' => $member->social_id,
            'vegetarian' => $member->vegetarian,
            'created_at' => $member->created_at,
            'nutritionist' => $member->level == 'patient' && $member->id != $member->parent_id && isset($parent) ? $parent->name : '',
            'recommander'  => $member->recommand_id > 0 ? Recommand::find($member->recommand_id)->name : ''
        ];
    }

}