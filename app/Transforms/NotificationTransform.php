<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/2/26
 * Time: 上午10:20
 */

namespace App\Transforms;
use App\Diet;
use App\Member;
use App\Track;
use Carbon\Carbon;


class NotificationTransform
{
    public function transformOther($collection,$read=0)
    {

        $result = collect();
        $collection->map(function($item,$key) use($result,$read){
            $oMember = Member::find($item->data['user_id']);
            $relative_id = $item->data['relative_id'];

            if (isset($item->data["is_edit"])) {
//                $row = $result->where('is_edit',$item->data["is_edit"])->where('user_unique',$oMember->code)->where('type',$item->data['type'])->first();
                $row = $result->where('is_edit',$item->data["is_edit"])->where('user_unique',$oMember->code)->first();
                $is_edit = $item->data["is_edit"];
            }else {
//                $row = $result->where('user_unique',$oMember->code)->where('type',$item->data['type'])->first();
                $row = $result->where('user_unique',$oMember->code)->first();
                $is_edit = 0;
            }

            $collect = collect();

            if ($row) {
                $row['badge'] = $read == 1 ? $row['badge'] : $row['badge'] + 1 ;
                $row['message_count'] = $row['message_count'] + 1 ;

                if (!in_array($item->data['type'],$row['type'])) {
                    $a = $row['type'];
                    array_push($a,$item->data['type']);
                    sort($a);
                    $row['type'] = $a;
                }

            }else {

                $parent_unique = 0;
                if ($oMember->level == 'patient' && $oMember->parent_id > 0) {
                    $oParentMember = Member::find($oMember->parent_id);
                    if ($oParentMember) {
                        $parent_unique = $oParentMember->code;
                    }
                }

                $show_image = null;
                $oDiet = Diet::withTrashed()->find($relative_id);
                if ( $oDiet ) {
                    if ($oDiet->photo != '') {
                        $show_image = storageURL($oDiet->photo);
                    }
                }

                $collect = collect([
                    'show_image' => $show_image,
                    'token'     => $item->id,
                    'user_img'  => storageURL($oMember->pic),
                    'user_role' => $oMember->level,
                    'user_name' => $oMember->name,
                    'user_unique'   => $oMember->code,
                    'relative_unique' => $relative_id,
                    'parent_unique' => $parent_unique,
                    'type'      => [$item->data['type']],
                    'is_edit'   => $is_edit,
                    'badge'     => $read == 1 ? 0 : 1,
                    'message_count' => 1,
                    'diff_time' => Carbon::parse($item->created_at)->diffForHumans()
                ]);

                $result[] = $collect;
            }

            return $collect;
        });

        return $result;
    }

    public function transformMy($collection,$user=null)
    {

        $result = collect();
        $collection->map(function($item) use ($result,$user){
            $track_approve = -99;
            $oMember = Member::find($item->data['user_id']);
            $relative_id = null;


            $show_image = null;


            switch($item->data['type']) {
                case 'track':
                case 'untrack':

                    if (isset($item->data['version']) && $item->data['version'] == '20180807') {
                        if ($item->data['type'] == 'track') {
                            $oTrack = Track::withTrashed()->find($item->data['relative_id']);
                            if ($oTrack) {
                                $track_approve = $oTrack->approve;
                                if (isset($oTrack->deleted_at) && $oTrack->approve == 0) {
                                    $track_approve = -99;
                                }
                            }
                        }
                    }else {
                        $oRelative = Member::find($item->data['relative_id']);
                        $relative_id = $oRelative->code;
                    }

                    break;
                case "track_deny":
                case "track_approve":
                    $relative_id = $item->data['relative_id'];
                    break;
                default:
                    $relative_id = $item->data['relative_id'];

                    $oDiet = Diet::withTrashed()->find($relative_id);
                    if ( $oDiet ) {
                        if ($oDiet->photo != '') {
                            $show_image = storageURL($oDiet->photo);
                        }
                    }

                    break;
            }

            if (isset($oMember)) {
                $result[] = $row = collect([
                    'show_image' => $show_image,
                    'token'     => $item->id,
                    'user_img'  => isset($oMember->pic) ? storageURL($oMember->pic) : '',
                    'user_role' => $oMember->level,
                    'user_name' => $oMember->name,
                    'user_unique'   => $oMember->code,
                    'relative_unique' => $relative_id,
                    'type'      => $item->data['type'],
                    'is_edit'   => isset($item->data['is_edit']) ? $item->data['is_edit'] : 0,
                    'badge'     => 1,
                    'message_count' => 1,
                    'diff_time' => Carbon::parse($item->created_at)->diffForHumans(),
                    'track_approve' => $track_approve
                ]);


                return $row;
            }
        });


        return $result;
    }
}