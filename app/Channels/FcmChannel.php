<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/7/6
 * Time: 上午10:30
 */
namespace App\Channels;

use Illuminate\Notifications\Notification;

class FcmChannel
{
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toFcm($notifiable);

    }
}