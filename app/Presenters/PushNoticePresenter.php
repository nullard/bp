<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2017/10/26
 * Time: 下午2:44
 */

namespace App\Presenters;


use Carbon\Carbon;

class PushNoticePresenter
{

    public function getType($value)
    {
        switch ($value) {
            case 'line':
                $output = 'Line';
                break;
            case 'mail':
                $output = 'Email';
                break;
            default:
                $output = '簡訊';
                break;
        }

        return $output;
    }

    public function getRoleType($value)
    {
        switch($value) {
            case 'member':
                $output = '受測者';
                break;
            case 'nutritionist':
                $output = '營養師';
                break;
            default:
                $output = '諮詢師';
                break;
        }

        return $output;
    }

    public function getTimeType($value)
    {

        switch ($value) {
            case 'once':
                $output = '一次性';
                break;
            case 'everyday':
                $output = '每天';
                break;
            case 'everyweek':
                $output = '每週';
                break;
            default: //everymonth
                $output = '每月';
                break;
        }

        return $output;
    }

    public function getMessageType($value)
    {
        switch ($value) {
            case 'text':
                $output = 'TEXT';
                break;
            case 'buttons':
                $output = 'Buttons';
                break;
            default:
                break;
        }

        return $output;
    }

    public function getStatus($value)
    {
        switch($value) {
            case 0:
                $output = '運行';
                break;
            case 1:
                $output = '運行完成';
                break;
            default: // -1
                $output = '暫停';
                break;
        }

        return $output;
    }

    public function getItemType($value)
    {
        switch($value) {
            case 'message':
                $output = '文字';
                break;
            case 'uri':
                $output = '連結';
                break;
            case 'postback':
                $output = '回傳代碼';
                break;
            default: // -1
                $output = '[請選擇]';
                break;
        }

        return $output;
    }

    public function getPredictTime($type,$desp,$time)
    {
        $output = '';
        $now = Carbon::now()->format('Y/m/d H:i');
        $now_date = Carbon::parse($now)->format('Y/m/d');
        $now_time = Carbon::parse($now)->format('H:i');
        $now_day = Carbon::parse($now)->dayOfWeek;

        switch($type) {
            case 'once':
                $predict = Carbon::parse($desp.' '.$time)->format('Y/m/d H:i');


                if ($predict > $now) {
                    $output = $predict;
                }else {
                    $output = '無下次通知';
                }
                break;
            case 'everyday':
                $time_array = explode(',',$time);
                $tmp = [];
                foreach($time_array as $item) {

                    $predicts[] = Carbon::parse($now_date.' '.$item)->format('Y/m/d H:i');
                    $predicts[] = Carbon::parse($now_date.' '.$item)->addDay()->format('Y/m/d H:i');

                    foreach($predicts as $predict) {
                        if ($predict > $now) {
                            $tmp[] = $predict;
                        }
                    }
                }

                if (count($tmp) > 0) {
                    $output = min($tmp);
                }
                break;
            case 'everyweek':

                $week_array = explode(',',$desp);
                foreach ($week_array as $key=>$item) {
                    if ($item == 7) {
                        $week_array[$key] = 0;
                    }
                }

                $time_array = explode(',',$time);

                for($i=0; $i<32; $i++) {
                    $date = Carbon::parse($now)->addDays($i)->format('Y/m/d');
                    $week = Carbon::parse($now)->addDays($i)->dayOfWeek;
                    if (in_array($week,$week_array)) {
                        $temp_week[] = $date;
                    }
                }

                $tmp = [];
                foreach($temp_week as $item_week) {

                    foreach($time_array as $item_time) {
                        $predict = Carbon::parse($item_week.' '.$item_time)->format('Y/m/d H:i');

                        if ($predict > $now) {
                            $tmp[] = $predict;
                        }
                    }
                }

                if (count($tmp) > 0) {
                    $output = min($tmp);
                }
                break;
            default: //everymonth
                $day_array = explode(',',$desp);
                $time_array = explode(',',$time);

                for($i=0; $i<32; $i++) {
                    $date = Carbon::parse($now)->addDays($i)->format('Y/m/d');
                    $day = Carbon::parse($now)->addDays($i)->format('d');
                    if (in_array($day,$day_array)) {
                        $temp_day[] = $date;
                    }
                }

                $tmp = [];
                foreach($temp_day as $item_day) {

                    foreach($time_array as $item_time) {
                        $predict = Carbon::parse($item_day.' '.$item_time)->format('Y/m/d H:i');

                        if ($predict > $now) {
                            $tmp[] = $predict;
                        }
                    }
                }

                if (count($tmp) > 0) {
                    $output = min($tmp);
                }

                break;
        }

        return $output;
    }

    public function listData($isJson)
    {
        $rs['type'] = [
            ['value'=>'line','text'=>$this->getType('line')],
            ['value'=>'mail','text'=>$this->getType('mail')],
            ['value'=>'text','text'=>$this->getType('text')],
        ];

        $rs['role_type'] = [
            ['value'=>'member', 'text'=> $this->getRoleType('member')],
            ['value'=>'nutritionist', 'text'=> $this->getRoleType('nutritionist')],
            ['value'=>'counselor', 'text'=> $this->getRoleType('counselor')],
        ];

        $rs['time_type'] = [
            ['value'=>'once', 'text'=> $this->getTimeType('once')],
            ['value'=>'everyday', 'text'=> $this->getTimeType('everyday')],
            ['value'=>'everyweek', 'text'=> $this->getTimeType('everyweek')],
            ['value'=>'everymonth', 'text'=> $this->getTimeType('everymonth')],
        ];

        $rs['message_type'] = [
            ['value'=>'text', 'text'=> $this->getMessageType('text')],
            ['value'=>'buttons', 'text'=> $this->getMessageType('buttons')],
        ];

        $rs['status'] = [
            ['value'=> 0, 'text'=> $this->getStatus(0)],
            ['value'=> 1, 'text'=> $this->getStatus(1)],
            ['value'=> -1, 'text'=> $this->getStatus(-1)],
        ];

        $rs['item_type'] = [
            ['value'=> '', 'text'=> $this->getItemType('')],
            ['value'=> 'message', 'text'=> $this->getItemType('message')],
            ['value'=> 'uri', 'text'=> $this->getItemType('uri')],
            ['value'=> 'postback', 'text'=> $this->getItemType('postback')],
        ];

        if ($isJson) {
            return json_encode($rs);
        }

        return $rs;
    }
}