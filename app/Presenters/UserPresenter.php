<?php
namespace App\Presenters;
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2017/10/20
 * Time: 上午10:51
 */
class UserPresenter
{
    public function getRole($value)
    {
        switch ($value) {
            case 'admin':
                $output = '網站管理員';
                break;
            case 'counselor':
                $output = '諮詢師';
                break;
            case 'nutritionist':
                $output = '營養師';
                break;
            default:
                $output = '尚未設定';
                break;
        }


        return $output;
    }
}