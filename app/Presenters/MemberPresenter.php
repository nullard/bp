<?php
namespace App\Presenters;
use App\MemberCalTarget;
use App\MemberWrecord;
use App\MemberWrecordItem;
use App\OrderPayment;
use App\User;
use Carbon\Carbon;
use App\Member;
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2017/10/20
 * Time: 上午10:51
 */
class MemberPresenter
{

    public function getAge($value)
    {

        if ($value != '') {
            $output = Carbon::parse($value)->diff(Carbon::now())->format('%y');
        }else {
            $output = '';
        }


        return $output;
    }

    public function getSex($value)
    {

        switch($value) {
            case '0':
                $output = '女';
                break;
            case '1':
                $output = '男';
                break;
            case '-2':
                $output = '未設定';
                break;
            default:
                $output = '';
                break;
        }

        return $output;
    }

    public function getLevel($level)
    {
        switch ($level) {
            case 'patient':
                $value = '一般會員';
                break;
            case 'nutritionist':
                $value = '營養師';
                break;
            default:
                $value = '未設定';
                break;
        }


        return $value;
    }

    public function getNutritionistList() {

        $rows = Member::where('level','nutritionist')->where('status',1)->get();

        return $rows;

    }

    public function getNutritionist($id) {

        $output = '待指派';

        if(!empty($id)) {
            $row = Member::find($id);
            if ($row) {
                $real_name = isset($row->real_name) ? $row->real_name : '';
                $output = $real_name.'('.$row->name.')';
            }else {
                $output = '不存在';
            }

        }

        return $output;
    }

    public function checkPayment($id) {

        $now = Carbon::now()->format('Y-m-d');

        $count = OrderPayment::where('member_id',$id)
            ->where('sdate','<=',$now)
            ->where('edate','>=',$now)
            ->where('status',1)->count();

        if ($count > 0) {
            return true;
        }

        return false;
    }

    public function getBMI($tall,$weight)
    {
        $output = 0;
        if ($tall > 0) {
            $result = $weight / pow( ($tall / 100),2);
            $output = round($result,1);
        }

        return $output;
    }

    public function getBMILevel($bmi)
    {
        $output = '';
        if ($bmi > 0 && $bmi < 18.5)  {
            $output = '體重過輕';
        }else if($bmi >= 18.5 && $bmi < 24) {
            $output = '正常範圍';
        }else if($bmi >= 24 && $bmi < 27) {
            $output = '過重';
        }else if($bmi >= 27 && $bmi < 30) {
            $output = '輕度肥胖';
        }else if($bmi >= 30 && $bmi < 35) {
            $output = '中度肥胖';
        }else if($bmi >= 35) {
            $output = '重度肥胖';
        }


        return $output;
    }

    function getEatType($value) {

        if ($value == 1) {
            return '素';
        }

        return '葷';

    }
}