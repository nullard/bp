<?php
namespace App\Presenters;
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2017/10/20
 * Time: 上午10:51
 */
class RecommandPresenter
{
    public function getType($value)
    {
        switch ($value) {
            case 'dr':
                $output = '醫生';
                break;
            case 'other':
                $output = '其他';
                break;
            default:
                $output = '尚未設定';
                break;
        }


        return $output;
    }
}