<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberCheckTask extends Model
{
    protected $fillable = [
        'check_date', 'member_wrecord_item_id','status','member_id'
    ];
}
