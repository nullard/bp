<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushNotice extends Model
{
    static public $redisKey = 'push-notice:list';
}
