<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MsgRead extends Model
{
    protected $fillable = [
        'user_id', 'member_id','chatroom_id','unread_count'
    ];

}
