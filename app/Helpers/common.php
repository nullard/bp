<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/2/13
 * Time: 上午11:45
 */
use Carbon\Carbon;


function storageURL($path) {

    if (preg_match("/^(http|https|\/uimages|\/uprofiles)/i", $path)) {
        return $path;
    } else {
        if ($path != '') {
            return '/storage-blob'.$path;
        }
        return '';
    }
}


function getWeek($sdate)
{
    $date = Carbon::parse($sdate);

    $weekStart = $date->startOfWeek();


    $cloneWeek = clone $weekStart;
    $period['sdate'] = $cloneWeek->format('Y-m-d');

    $weekEnd = $weekStart->addDays(6);
    $period['edate'] = $weekEnd->format('Y-m-d');


    return $period;
}


function getCaloria($sugar,$protein,$fat) {

    return round($protein * \App\Services\NutriProportion::$protein + $fat * \App\Services\NutriProportion::$fat + $sugar * \App\Services\NutriProportion::$sugar);
}