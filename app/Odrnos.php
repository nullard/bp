<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Odrnos extends Model
{
    protected $fillable = [
        'prefix', 'middle', 'serial'
    ];

    function generateNO($prefix,$middle) {


        DB::beginTransaction();

        $row = $this->where('prefix',$prefix)
            ->where('middle',$middle)->lockForUpdate()->first();


        if ($row) {

            $serial = $row->serial + 1;

            $row->serial = $serial;
            $row->update();

        }else {

            $serial = 1;

            $this->prefix = $prefix;
            $this->middle = $middle;
            $this->serial = $serial;
            $this->save();

        }

        DB::commit();

        $no = $prefix.$middle.str_pad($serial,5,'0',STR_PAD_LEFT);

        return $no;
    }
}
