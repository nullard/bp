<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    protected $fillable = ['diet_id','member_id','message'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
