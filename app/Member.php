<?php

namespace App;


use App\Presenters\MemberPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Notifications\Notifiable;

class Member extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    protected $guarded = 'front';
    protected $fillable = [
        'user_id', 'name','real_name','birth','sex','type','social_id','pic','status','vegetarian','code','email','recommand_id','parent_id','password'
    ];

    protected $appends = [
        'age'
    ];

    public function user() {
        return $this->belongsTo('App\User')->withDefault();
    }

    public function parent()
    {
        return $this->belongsTo('App\Member', 'parent_id');
    }

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    public function recommand()
    {
        return $this->hasOne('App\Recommand','id','recommand_id');
    }

    public function latestKgTarget() {
            return $this->hasMany('\App\MemberKgTarget','member_id','id')->latest()->limit(1);
    }

    public function chatroom()
    {
        return $this->morphOne('App\Chatroom', 'fromtable');
    }

    function lastMessage() {

        return $this->hasOne('App\Message','id','last_message_id');
    }

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public function getDataByToken($token) {

        $social_id =  base64_decode($token);
        return self::with('latestKgTarget')->where('social_id',$social_id)->first();
    }

    public static function getDataByCode($code) {

        return self::where('code',$code)->first();

    }

    public function getAgeAttribute()
    {
        return (new MemberPresenter)->getAge($this->birth);
    }
}
