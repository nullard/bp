<?php

namespace App;

use App\DietLog;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Diet extends Model
{
    use SoftDeletes;

    public static function boot(){

        parent::boot();

        static::created(function($model) {

            if ($model->type == 'bp') {
                MemberDrecord::updateBp('create',
                    $model->member_id,
                    Carbon::parse($model->meal_time)->format('Y-m-d'),
                    $model->sbp,
                    $model->dbp,
                    $model->id);
            }

        });

        static::updating(function($model)
        {
            $original = $model->getOriginal();


            if ($original['type'] == 'caloria') {
                MemberDrecord::updateCaloria($original['member_id'],Carbon::parse($original['meal_time'])->format('Y-m-d'), $original['value']*-1);
            }

            if($model->type == 'caloria') {
                MemberDrecord::updateCaloria($model->member_id,Carbon::parse($model->meal_time)->format('Y-m-d'), $model->value);
            }

            $originLog = DietLog::where('diet_id',$model->id)->latest()->first();
            $token = md5($original['protein'].$original['fat'].$original['sugar'].$original['name'].$original['remark'].$original['type'].$original['value'].$original['parent_member_id']);
            if (!isset($originLog) || $token != $originLog->verify_token) {
                $log = new DietLog();
                $log->diet_id = $model->id;
                $log->verify_token = $token;
                $log->content = json_encode($original);
                $log->save();
            }

        });

        static::updated(function ($model)
        {
            $original = $model->getOriginal();

            if($original['type'] == 'bp') {
                MemberDrecord::updateBp('update',$model->member_id,Carbon::parse($original['meal_time'])->format('Y-m-d'), 0, 0, $original['id']);
            }

            if ($model->type == 'bp') {
                MemberDrecord::updateBp('update',$model->member_id,Carbon::parse($model->meal_time)->format('Y-m-d'), $model->sbp, $model->dbp, $model->id);
            }

        });

        static::deleting(function($model) {

            if ($model->type == 'caloria') {
                MemberDrecord::updateCaloria($model->member_id,Carbon::parse($model->meal_time)->format('Y-m-d'), $model->value * -1);
            }else if ($model->type == 'bp') {
                MemberDrecord::updateBp('delete',$model->member_id,Carbon::parse($model->meal_time)->format('Y-m-d'), 0, 0, $model->id);
            }

        });


    }

    public function member() {
        return $this->belongsTo('App\Member');
    }

    public function parent_member() {
        return $this->belongsTo('App\Member','parent_member_id');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
    
    static public function dietShare($user)
    {
        return self::where(function($query) use($user){

            if ($user && $user->is_admin == 1 && $user->god_vw == 1) {
                //god view
            }else {
                //public
                $query->where('share_type','public');
                if ($user) {
                    //自己
                    $query->orwhere('member_id',$user->id);
                    //專家
                    $query->orwhere('share_type','expert')->whereHas('member',function($query) use($user){
                        $query->where('parent_id',$user->id);
                    });
                    //追蹤的人
                    $query->orwhere('share_type','tracker')->whereHas('member',function($query) use($user){
                        $query->leftJoin('tracks','members.id','=','tracks.track_member_id')
                            ->where('tracks.member_id',$user->id)
                            ->whereNull('tracks.deleted_at')
                            ->where('approve',1);
                    });
                }
            }

        });
    }

    static public function getNotRateData($member_id,$sdate) {

        $user = Auth::guard('front')->user();

        $data = self::dietShare($user)
            ->where('member_id',$member_id)
            ->where('created_at','>=',$sdate)
            ->where('parent_member_id',0)
            ->where('type','caloria')
            ->first();

        return $data;
    }

    static public function notRateCountWithFood($member_id,$sdate)
    {

        $user = Auth::guard('front')->user();

        $count = self::dietShare($user)
            ->where('member_id',$member_id)
            ->where('created_at','>=',$sdate)
            ->where('parent_member_id',0)
            ->where('type','caloria')
            ->count();

        return $count;
    }

}
