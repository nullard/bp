<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/2/12
 * Time: 下午5:04
 */

namespace App\Services;

use Confing;

class LineService
{

    function requestLineServer($code) {

        $output['status'] = "FAIL";

        $curl = curl_init();

        $authURL = 'https://api.line.me/oauth2/v2.1/token';

        $data = array(
            'grant_type' => 'authorization_code',
            'client_id' => Config('services.line.client_id'),
            'client_secret' => Config('services.line.client_secret'),
            'code' => $code,
            'redirect_uri' => Config('services.line.redirect')
        );

        curl_setopt($curl, CURLOPT_URL, $authURL);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


        $rs = curl_exec($curl);
        if (curl_errno($curl)) {
            Log::error(curl_error($curl));
            $output['msg'] = curl_error($curl);
        }
        curl_close($curl);

        $decode = json_decode($rs);

        if (!isset($decode->error)) {
            $output['status'] = "OK";
            $output['data'] = $decode;
        }else {
            $output['msg'] = $decode->error_description;
        }


        return $output;

    }

    function paserJWTFromLineResponse($result,$type='line') {

        $data['status'] = "FAIL";

        if (isset($result->id_token)) {

            $jwtString = $result->id_token;

            $parts = explode('.', $jwtString);

            $signWith = implode('.', array($parts[0], $parts[1]));
            $key = Config('services.line.client_secret');
            $verySignature = hash_hmac('SHA256', $signWith, $key, true);
            $signature = $this->base64Decode($parts[2]);

            if ($signature === $verySignature) {
                $header = $this->base64Decode($parts[0]);
                $body = $this->base64Decode($parts[1]);
                $headerDecoded = json_decode($header);
                $bodyDecoded = json_decode($body);

                $social_id = $bodyDecoded->sub;

                $data['status'] = 'OK';
                $std = new \stdClass();
                $std->user['id'] = $social_id;
                $std->user['nickname'] = $bodyDecoded->name;
                $std->user['name'] = $bodyDecoded->name;
                $std->user['email'] = null;
                $std->avatar_original = $bodyDecoded->picture;


                $data['user'] = $std;

            }else {
                $data['msg'] = '請求來源不正確';
            }

        }else {
            $data['msg'] = 'id token 不存在';
        }

        return $data;

    }

    function base64Decode($string)
    {
        $decoded = str_pad($string,4 - (strlen($string) % 4),'=');
        return base64_decode(strtr($decoded, '-_', '+/'));
    }
}