<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2017/12/18
 * Time: 下午2:56
 */

namespace App\Services;


abstract class NutriProportion
{
    static $protein = 75;
    static $sugar = 70;
    static $fat = 45;
}