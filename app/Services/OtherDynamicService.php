<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/2/26
 * Time: 上午11:16
 */

namespace App\Services;


use App\Member;
use App\Notifications\OtherDynamic;
use App\Track;

class OtherDynamicService
{
    public function notification($member,$oDiet,$isEdit)
    {

        $oTrackList = null;

        if(in_array($oDiet->share_type, ['public','tracker']))
        {
            $oTrackList = Track::where('track_member_id',$member->id)
                ->where('approve',1)
                ->get();

        }else if($oDiet->share_type == 'expert') {

            $oTrackList = Track::where('member_id',$member->parent_id)
                                ->where('track_member_id',$member->id)
                                ->where('approve',1)
                                ->get();

        }

        if ($oTrackList) {
            foreach($oTrackList as $track) {
                $oMember = Member::find($track->member_id);
                $oMember->notify(new OtherDynamic($member,$oDiet,$isEdit));
            }
        }


    }

}