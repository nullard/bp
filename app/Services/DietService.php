<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/2/6
 * Time: 下午4:05
 */

namespace App\Services;


use App\Calorie;
use App\Comment;
use App\Diet;
use App\Like;
use App\Track;
use Auth;

class DietService
{
    function __construct()
    {

    }

    public function transformData(Diet $item,$proportion=true)
    {
        $user = Auth::guard('front')->user();

        $track = -99;
        $like = false;
        if ($user) {
            $oTrack = Track::where('member_id',$user->id)->where('track_member_id',$item->member->id)->first();

            if ($oTrack) {
                $track = $oTrack->approve;
            }

            $like = Like::where('member_id',$user->id)->where('diet_id',$item->id)->count() > 0 ? true : false;
        }

        $row['status'] = 200;
        $row['like'] = $like;
        $row['track'] = $track;
        $row['paid'] = true;
        $row['like_count'] = Like::where('diet_id',$item->id)->count();
        $value = $item->value;
        switch($item->type) {
            case 'sport':
                $type = 'life';
                break;
            case 'caloria':
                if ($item->parent_member_id == 0) {
                    $row['paid'] = false;
                }

                $type = $item->type;
                $total = $this->getCaloria($item->sugar,$item->protein,$item->fat);

                if ($item->calorie_id > 0) {
                    $oCalorie = Calorie::find($item->calorie_id);
                    $value = $oCalorie->value;
                }

                if ($proportion) {
                    $row['sugar'] = $total > 0 ? round(100 * ($item->sugar * NutriProportion::$sugar) / $total, 0)  : 0;
                    $row['fat'] = $total > 0 ? round( 100 * ($item->fat * NutriProportion::$fat) / $total,0) : 0;
                    $row['protein'] = $total > 0 ? round( 100 * ($item->protein * NutriProportion::$protein) / $total,0) : 0;
                }else {
                    $row['sugar'] = $item->sugar;
                    $row['fat'] = $item->fat;
                    $row['protein'] = $item->protein;
                }

                $row['rated'] = $item->parent_member_id > 0 ? 1 : 0; //已評分

                break;
            case 'bp':
                $type = $item->type;
                $row["sbp"] = $item->sbp;
                $row["dbp"] = $item->dbp;


                break;
            default:
                $type = 'life';
                break;
        }
        $row['unique'] = $item->id;
        $row['type'] = $type;
        $row['photo'] = storageURL($item->photo);
        $row['user_name'] = $item->member->name;
        $row['user_role'] = $item->member->level;
        $row['user_image'] = storageURL($item->member->pic);
        $row['user_unique'] = $item->member->code;
        $row['created_at'] = $item->created_at->diffForHumans();
        $row['name'] = $item->name;
        $row['value'] = $value;
        $row['meal_time'] = $item->meal_time;
        $row['read'] = $item->read;
        $row['desp'] = isset($item->desp) ? $item->desp : "";
        $row['remark'] = isset($item->remark) ? $item->remark : "";
        $row['share_type'] = $item->share_type;
        $row['is_moon'] = $item->is_moon;

        $isCheck = false;
        if ($user) {
            if ($user->level == 'nutritionist') {
                if ($user->id == $item->parent_member_id) {
                    $isCheck = true;
                }
            }else {
                if ($user->id == $item->member->id) {
                    if ($item->parent_member_id > 0 && $item->read == 0) {
                        $isCheck = true;
                    }
                }
            }
        }


        $row['check'] = $isCheck;


        if (!empty($item->desp)) {
            $row['comments'][] = [
                'user_unique'=>$item->member->code,
                'user_name'=>$item->member->name,
                'user_role'=>$item->member->level,
                'message'=> $item->desp,
                'type'  => 'diet',
                'unique'    => $item->id
            ];
        }else {
            $row['comments'] = [];
        }

        if (!empty($item->remark) && isset($item->parent_member)) {
            $row['comments'][] = [
                'user_unique'=> $item->parent_member->code,
                'user_name'=> $item->parent_member->name,
                'user_role'=>$item->parent_member->level,
                'message'=> $item->remark,
                'type'  => 'diet_remark',
                'unique'=> $item->id
            ];
        }


        $comment_collection = Comment::where('diet_id',$item->id)->oldest()->get();
        $commnet_othters  = $comment_collection->map(function($item) {
            return [
                'user_unique'   => $item->member->code,
                'user_name'   =>  $item->member->name,
                'user_role'   => $item->member->level,
                'message'     =>  $item->message,
                'type'        => 'comment',
                'unique'    => $item->id
            ];
        })->toArray();
        $row['comments'] = array_merge($row['comments'],$commnet_othters);

        return $row;
    }

    function getCaloria($sugar,$protein,$fat) {

        return round($protein * NutriProportion::$protein + $fat * NutriProportion::$fat + $sugar * NutriProportion::$sugar);

    }
}