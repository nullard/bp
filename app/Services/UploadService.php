<?php
/**
 * Created by PhpStorm.
 * User: nulla
 * Date: 2018/2/12
 * Time: 下午3:03
 */

namespace App\Services;

use Storage;
use Illuminate\Support\Facades\File;
use Image;

abstract class So88Upload {
    public static function rootPath() {
        return realpath(base_path().'/../storage/bp');
    }
}

class UploadService{

    function fromURL($url,$saveDir,$width=null,$height=null) {

        //$extension = pathinfo($url, PATHINFO_EXTENSION);
        $size = getimagesize($url);
        $extension = image_type_to_extension($size[2]);
        $filename = $this->filename().$extension;

        $file = file_get_contents($url);

        if(!is_dir($saveDir)) {
            File::makeDirectory($saveDir, $mode = 0777, true, true);
        }
        $save = file_put_contents($saveDir.$filename,$file);

        $output_file = $saveDir.$filename;
        if ($save) {
            $this->resize($output_file,$width,$height);
            return $filename;
        }

        return false;
    }

    function fromBase64($base64_string, $saveDir, $width=null, $height=null) {

        if(!is_dir($saveDir)) {
            File::makeDirectory($saveDir, $mode = 0777, true, true);
        }
        $filename = $this->filename().'.jpg';
        $output_file = $saveDir.$filename;
        $ifp = fopen( $output_file, 'wb' );

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        // clean up the file resource
        fclose( $ifp );

        $this->resize($output_file,$width,$height);
        return $filename;

        return $filename;
    }

    function filename() {
        return str_random(4).str_random(4);
    }

    function resize($filePath,$width=null,$height=null) {

        $img = Image::make($filePath);
        $img->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save($filePath);

        return true;

    }

}