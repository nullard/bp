<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class MemberLevel extends Model
{
    use NodeTrait;

    protected $fillable = [
        'name', 'member_id'
    ];

    public static function createRelation(Member $parent,Member $child)
    {

        if ($parent && $parent->status == 1) {
            if (self::where('member_id',$parent->id)->count() == 0) {
                $parent = new MemberLevel(['member_id'=>$parent->id,'name'=>$parent->name]);
                $parent->save();
            }else {
                $parent = self::where('member_id',$parent->id)->first();
            }

            if (self::where('member_id',$child->id)->count() == 0) {
                $parent->children()->create(['member_id'=>$child->id,'name'=>$child->name]);
            }
        }else {
            if (self::where('member_id',$child->id)->count() == 0) {
                $parent = new MemberLevel(['member_id'=>$child->id,'name'=>$child->name]);
                $parent->save();
            }
        }

    }
}
