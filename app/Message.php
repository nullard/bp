<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function chatroom()
    {
        return $this->belongsTo('App\Chatroom');
    }
/*
    public function getCreatedAtAttribute($value) {

        return \Carbon\Carbon::parse($value)->format('m/d h:m');
    }*/
}
