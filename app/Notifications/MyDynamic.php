<?php
/*
 *
 * comment like track
 *
 * */
namespace App\Notifications;

use App\Device;
use App\Diet;
use App\Member;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MyDynamic extends Notification implements ShouldQueue
{
    use Queueable;

    var $fromMember;
    var $relative_id;
    var $type;
    var $isEdit;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($fromMember,$type,$relative_id=null,$isEdit=0)
    {
        $this->fromMember = $fromMember;
        $this->type = $type;
        $this->relative_id = $relative_id;
        $this->isEdit = $isEdit;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $rs = ['user_id'  => $this->fromMember->id,
               'type'       => $this->type,
               'is_edit'    => $this->isEdit,
               'version' => '20180807'
            ];

        if ($this->relative_id) {
            $rs['relative_id'] = $this->relative_id;
        }

        if ($notifiable->id == $this->fromMember->id) {
            exit;
        }

        $this->pushMobile($notifiable->id);

        return $rs;
    }

    public function pushMobile($to)
    {

        $res = $this->typeOfName();

        \Notification::send(Member::find($to)->first(),new FcmNotification($to,$res['title'],$res['body']));

    }

    public function typeOfName()
    {

        $body = '';
        if ($this->relative_id) {
            if (in_array($this->type,['comment','like','unlike','rated'])) {
                $oDiet = Diet::find($this->relative_id);
                if ($oDiet) {
                    $body = $oDiet->name;
                }
            }else if (in_array($this->type,['track','untrack','track_approve','track_deny'])) {
                if ($this->fromMember) {
                    $title = $this->fromMember->name;
                }
            }
        }

        switch($this->type) {
            case 'track':
                $body = '追蹤你';
                break;

            case 'track_approve':
                $body = '同意你的追蹤';
                break;

            case 'track_deny':
                $body = '拒絕你的追蹤';
                break;

            case 'untrack':
                $body = '取消追蹤你';
                break;

            case 'comment':
                $title = '您有新留言';
                break;

            case 'like':
                $title = '您有新的喜歡';
                break;

            case 'unlike':
                $title = '您的喜歡已被取消';
                break;

            case 'rated':
                $title = '您的照片'.( $this->isEdit ? '已修改評分' : '已評分');
                break;

            default:
                $title = $this->type;
                break;
        }


        return [
            "title" => $title,
            "body"  => $body
        ];


    }


}
