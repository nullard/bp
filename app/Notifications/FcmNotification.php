<?php

namespace App\Notifications;

use App\Channels\FcmChannel;
use App\Device;
use App\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FcmNotification extends Notification
{
    use Queueable;

    public $to;
    public $title;
    public $body;
    public $badge;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($to,$title,$body)
    {
        $this->to = $to;
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FcmChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toFcm($notifiable)
    {

        $deviceList = Device::where('active',1)->where('member_id',$this->to)->get();
        $badge = count(Member::find($this->to)->unreadNotifications) + 1;

        foreach ($deviceList as $device) {

            $deviceToken = $device->push_token;

            \Edujugon\PushNotification\Facades\PushNotification::setService('fcm')
                ->setMessage([
                    'notification' => [
                        'title'=> $this->title,
                        'body'=> $this->body,
                        'sound' => 'default',
                        'badge' => $badge
                    ]
                ])
                ->setDevicesToken([$deviceToken])
                ->send()
                ->getFeedback();
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
