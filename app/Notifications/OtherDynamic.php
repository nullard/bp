<?php

namespace App\Notifications;

use App\Device;
use App\Member;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OtherDynamic extends Notification
{
    use Queueable;

    var $user;
    var $oDiet;
    var $isEdit;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$oDiet,$isEdit)
    {
        $this->user = $user;
        $this->oDiet = $oDiet;
        $this->isEdit = $isEdit;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {

        if ($notifiable->id == $this->user->parent_id) {
            $this->pushMobile($notifiable->id);
        }


        return [
            'user_id' => $this->user->id,
            'type' => $this->oDiet->type,
            'relative_id' => $this->oDiet->id,
            'is_edit' => $this->isEdit
        ];
    }


    public function pushMobile($to)
    {

        $res = $this->typeOfName();

        \Notification::send(Member::find($to)->first(),new FcmNotification($to,$res['title'],$res['body']));

    }

    public function typeOfName()
    {

        $title = $this->user->name;

        switch($this->oDiet->type) {
            case 'caloria':
                $body = '上傳了食物照片';
                break;

            case 'bp':
                $body = '上傳了血壓';
                break;

            case 'life':
                $body = '上傳了生活照';
                break;

            default:
                $body = 'none';
                break;
        }


        return [
            "title" => $title,
            "body"  => $body
        ];


    }
}
