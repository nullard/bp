<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Member;

class NewMemberActive extends Mailable
{
    use Queueable, SerializesModels;
    var $member;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("瘦88會員認證信")
            ->from('admin@so88.com.tw')
            ->view('emails.members.new_member_active');
    }
}
