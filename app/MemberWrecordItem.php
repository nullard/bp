<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberWrecordItem extends Model
{
    protected $fillable = [
        'member_id','type','desp','member_wrecord_id'
    ];
}
