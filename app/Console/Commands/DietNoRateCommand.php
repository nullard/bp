<?php

namespace App\Console\Commands;

use App\Diet;
use App\Mail\Template;
use App\Member;
use App\Notifications\FcmNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DietNoRateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'diet:noRate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '圖片超過24小時以上未評估，通知系統';

    var $hours = 24;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Carbon::setLocale('zh-tw');

        $isProduction = config('app.env') == 'production' ? true : false;

        $subject = ($isProduction ? '[系統通知]':'[TEST系統通知]').'營養師超過'.$this->hours.'小時以上未評估照片';
        $content = "名單如下：\n";

        $rows = Diet::whereIn('share_type',['public','expert','tracker'])
                ->select('member_id')->where('parent_member_id',0)->where('type','caloria')->whereIn('member_id',function($query) {
            $query->select('id')
                ->from(with(new Member)->getTable())
                ->where('level','patient')
                ->where('parent_id','>',0)
                ->where('status',1);
        })->where('created_at','<=', Carbon::now()->addHour($this->hours * -1)->format('Y-m-d H:i:s'))
            ->groupBy('member_id')->get()->pluck('member_id');

        $data = [];
        foreach($rows as $member_id) {
            $info = [];

            $oMember = Member::with('parent')->find($member_id);
            $firstDiet = Diet::dietShare($oMember->parent)->where('parent_member_id',0)->where('type','caloria')->where('member_id',$oMember->id)->orderBy('created_at', 'asc')->first();
            $counts = Diet::dietShare($oMember->parent)->where('parent_member_id',0)->where('type','caloria')->where('member_id',$oMember->id)->count();


            $info['name'] = isset($oMember->real_name) ? $oMember->real_name : $oMember->name;
            $info['count'] = $counts;
            $info['human_time'] = Carbon::parse($firstDiet->created_at)->diffForHumans(Carbon::now());

            $parent_member_id = $oMember->parent_id;

            if (isset($parent_member_id)) {
                if(!isset($data[$parent_member_id])) {
                    $data[$parent_member_id]['name'] = isset($oMember->parent->real_name) ? $oMember->parent->real_name : $oMember->parent->name;
                }
                $data[$parent_member_id]['item'][] = $info;
            }
        }

        if (count($data) > 0) {

            foreach($data as $key=>$row) {
                $content .= "##".$row['name']."\n";
                foreach($row['item'] as $item) {
                    $content .= "### &nbsp;&nbsp;&nbsp;".$item['name'].' - 共計'.$item['count']."張未處理(".$item['human_time'].")\n";
                }

                \Notification::send(Member::find($key)->first(),new FcmNotification($key, '未評分通知', '再請撥空前往評分，謝謝'));
            }

            if ($isProduction) {
                Mail::to('nulla@nulla.com.tw')->send(new Template($subject,$content));
            }else {
                Mail::to('fm@nulla.com.tw')->send(new Template($subject,$content));
            }

        }

    }
}
