<?php

namespace App\Console\Commands;

use App\Member;
//use App\Notifications\MemberLineNotification;
use App\Order;
use App\PaymentLog;
use App\OrderPayment;
use App\RecommandResult;
use App\RecommandResultItem;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\So88Product;

class GetCreditPeriodInfo extends Command
{
    use Notifiable;
    var $debug = true;
    var $oEcpay;
    var $oInvoice;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getCreditPeriod:info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '有三筆成功定期定額付款，沒有帶return url 參數，需做修補';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (config('app.env') == 'production') {
            $this->debug = false;
        }
    }

    function instance($instanceClass=null) {

        if (\ECPay_AllInOne::class == $instanceClass) {
            try {
                $this->oEcpay = new \ECPay_AllInOne();
                $this->oEcpay->ServiceURL  = $this->debug ? 'https://payment-stage.ecpay.com.tw/Cashier/QueryCreditCardPeriodInfo' : 'https://payment.ecpay.com.tw/Cashier/QueryCreditCardPeriodInfo';
                $this->oEcpay->HashKey     = $this->debug ? config('services.ecpay.dev.hashkey') : config('services.ecpay.release.hashkey');
                $this->oEcpay->HashIV      = $this->debug ? config('services.ecpay.dev.hashiv') : config('services.ecpay.release.hashiv');
                $this->oEcpay->MerchantID  = $this->debug ? config('services.ecpay.dev.merchantid') : config('services.ecpay.release.merchantid');
                $this->oEcpay->EncryptType = '1';

            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        if (\EcpayInvoice::class == $instanceClass) {
            try {
                $this->oInvoice = new \EcpayInvoice();
                $this->oInvoice->Invoice_Url = $this->debug ? config('services.ecpay.dev.invoice_service_url') : config('services.ecpay.release.invoice_service_url');
                $this->oInvoice->HashKey = $this->debug ? config('services.ecpay.dev.invoice_hashkey') : config('services.ecpay.release.invoice_hashkey');
                $this->oInvoice->HashIV = $this->debug ? config('services.ecpay.dev.invoice_hashiv') : config('services.ecpay.release.invoice_hashiv');
                $this->oInvoice->MerchantID  = $this->debug ? config('services.ecpay.dev.merchantid') : config('services.ecpay.release.merchantid');
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order_ids = [5,8,11];


        if(date('Y') != '2018') {
            return;
        }

        foreach($order_ids as $order_id) {

            try
            {
                $oOrder = Order::find($order_id);


                $this->instance(\ECPay_AllInOne::class);
                $this->oEcpay->Query = array(
                    'MerchantTradeNo' => $oOrder->odr_no, 'TimeStamp' => time() + 5 * 60 * 60
                );

                $arFeedback = $this->oEcpay->QueryPeriodCreditCardTradeInfo();

                Log::info('order_period_check => '.json_encode($arFeedback));
                if (sizeof($arFeedback) > 0) {
                    foreach ($arFeedback as $key => $value) {
                        switch ($key)
                        {
                            /* 支付後的回傳的基本參數 */
                            case "RtnCode": $szRtnCode = $value; break;
                            case "ExecLog": $szExecLog = $value; break;
                            case "TotalSuccessTimes": $szTotalSuccessTimes = $value; break;
                            default: break;
                        }
                    }

                    $this->setLog('order_period_check',$oOrder,$arFeedback);

                    $execLogLastIndex = count($szExecLog) - 1;

                    if ($execLogLastIndex >= 0) {
                        $exec = $szExecLog[$execLogLastIndex];
                        $parse = substr($exec['process_date'],0,10);
                        $parse_result = Carbon::parse($parse)->format('Y-m-d');

                        if (Carbon::parse($parse)->endOfMonth()->format('Y-m-d') == $parse_result) {
                            $edate = Carbon::parse($parse)->addDay(1)->endOfMonth()->addDay(-1)->format('Y-m-d');
                        }else {
                            $edate = Carbon::parse($parse)->addMonth()->addDay(0)->format('Y-m-d');
                        }

                        $oOrderPayment = OrderPayment::updateOrCreate(
                            ['order_id'=>$oOrder->id,'member_id'=>$oOrder->member_id,'pay_date'=>$parse_result,'sdate'=>$parse_result,'edate'=>$edate],
                            ['total' => $oOrder->total]
                        );

                        if ($szRtnCode == 1) {

                            if ($oOrderPayment->status != 1) {

                                $oOrderPayment->status = 1;
                                $oOrderPayment->save();


                                if ($oOrder->total_success_times < $szTotalSuccessTimes) {
                                    $oOrder->total_success_times = $szTotalSuccessTimes;
                                    $oOrder->save();
                                }

                                $this->createInvoice($oOrder,$oOrderPayment);
                            }
                        }else {

                            //$preload = ['oOrderPayment'=>$oOrderPayment];
                            //$this->notify(new MemberLineNotification('PAY_FAIL','text',$oMember->social_id,$preload));

                            //exit;
                        }
                    }



                }


            } catch (Exception $e) {
                $error = '0|' . $e->getMessage();

                $this->setLog('order_create_fail','0',$error);
            }

        }

    }

    function createInvoice($order,$oOrderPayment) {




        if (empty($oOrderPayment->invoice_number)) {

            $oMember = Member::with('latestKgTarget')->find($order->member_id);

            if ($oMember->recommand_id > 0) {
                $oRememberResult = RecommandResult::updateOrCreate(
                    ['recommand_id'=>$oMember->recommand_id,'recommand_date'=>date('Y-m-d')]
                );
                $oRememberResult->member_pay_count = $oRememberResult->member_pay_count + 1;
                $oRememberResult->save();

                $oRecommandItem = new RecommandResultItem();
                $oRecommandItem->recommand_result_id = $oRememberResult->id;
                $oRecommandItem->type = 'add_member';
                $oRecommandItem->fromtable_id = $oOrderPayment->id;
                $oRecommandItem->fromtable_type = 'order_payments';
                $oRecommandItem->save();
            }


            $this->instance(\EcpayInvoice::class);

            $this->oInvoice->Invoice_Method = \EcpayInvoiceMethod::INVOICE;

            array_push($this->oInvoice->Send['Items'], So88Product::products($order->total,1));

            $this->oInvoice->Send['RelateNumber'] = $order->odr_no.'-'.$oOrderPayment->id;
            $this->oInvoice->Send['CustomerName'] = $order->name;
            $this->oInvoice->Send['CustomerEmail'] = $order->email;
            $this->oInvoice->Send['Print'] = \ECPay_PrintMark::No;
            $this->oInvoice->Send['Donation'] = \ECPay_Donation::No;
            $this->oInvoice->Send['TaxType'] = \ECPay_TaxType::Dutiable;
            $this->oInvoice->Send['SalesAmount'] = $order->total;
            $this->oInvoice->Send['InvoiceRemark'] = '';
            $this->oInvoice->Send['InvType'] = \ECPay_InvType::General;

            $aReturn_Info = $this->oInvoice->Check_Out();

            $this->setLog('invoice_create', $order, $aReturn_Info, 0);

            foreach ($aReturn_Info as $key => $value) {
                switch ($key) {
                    case "RtnCode":
                        $iv_RtnCode = $value;
                        break;
                    case "RtnMsg":
                        $iv_RtnMsg = $value;
                        break;
                    case "InvoiceNumber":
                        $iv_InvoiceNumber = $value;
                        break;
                    case "InvoiceDate":
                        $iv_InvoiceDate = $value;
                        break;
                    case "RandomNumber":
                        $iv_RandomNumber = $value;
                        break;
                    case "CheckMacValue":
                        $iv_CheckMacValue = $value;
                        break;
                    default:
                        break;
                }
            }

            $oOrderPayment->invoice_number = $iv_InvoiceNumber;
            $oOrderPayment->invoice_date = $iv_InvoiceDate;
            $oOrderPayment->invoice_rand_number = $iv_RandomNumber;
            $oOrderPayment->save();


/*
            $preload = ['oOrderPayment'=>$oOrderPayment];
            $this->notify(new MemberLineNotification('PAY_FINISHED','text',$oMember->social_id,$preload));


            $preload = ['oMember' => $oMember];
            $isNotify = false;

            if (count($oMember->latestKgTarget) == 0) {
                $isNotify = true;
            }else {
                if ($oMember->latestKgTarget[0]->weight == 0.0) {
                    $isNotify = true;
                }
            }

            if ($isNotify) {
                $this->notify(new MemberLineNotification('FILLED_INFO','buttons',$oMember->social_id,$preload));
            }
*/
        }
    }

    function setLog($type,$order,$desp,$simulate=0) {

        $o = new PaymentLog();
        $o->type = $type;
        $o->third_party = 'ecpay';
        $o->order_id = $order->id;
        $o->desp = json_encode($desp);
        $o->simulate = $simulate;
        $o->save();

    }
}
