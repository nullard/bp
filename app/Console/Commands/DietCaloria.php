<?php

namespace App\Console\Commands;

use App\Calorie;
use App\Diet;
use App\Member;
use App\Notifications\MyDynamic;
use Illuminate\Console\Command;
use Goutte\Client;
use Illuminate\Support\Facades\Log;

class DietCaloria extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'diet:caloria {food} {diet_id} {notification=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'UA 網頁抓食物熱量';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //source https://github.com/FriendsOfPHP/Goutte
        $search = $this->argument('food');
        $diet_id = $this->argument('diet_id');
        $isNotification = $this->argument('notification');

        if (empty($search)) {
            return false;
        }

        $rowSearch = Calorie::where('name',$search)->first();

        if (!$rowSearch) {
            $encode = urlencode($search);

            $client = new Client();
            $crawler = $client->request('GET', "https://www.myfitnesspal.com/zh-TW/food/search?page=1&search=".$encode);
            $data = [];
            $crawler->filter('#new_food_list .food_search_results li')->each(function ($node) use(&$data){
                //name
                $row = new \stdClass();

                $node->filter('.food_info .food_description a')->each(function($subNode,$i) use (&$row) {
                    if ($i == 0) {
                        $row->name = $subNode->text();
                    }else {
                        $row->brand = $subNode->text();
                    }
                });

                //value
                $node->filter('.food_info .nutritional_info')->each(function($subNode) use (&$row){
                    $items = explode(",",$subNode->text());
                    foreach($items as $key=>$item) {
                        if ($key == 0 || $key == 1) {
                            $values = explode(":",$item);
                            if (isset($values[1])) {
                                $value = trim($values[1]);
                                if ($key == 0) {
                                    $row->unit = $value;
                                }else {
                                    $row->value = $value;
                                }
                            }else {
                                if ($key == 0) {
                                    $row->unit = '';
                                }else {
                                    $row->value = 0;
                                }
                            }
                        }
                    }
                });

                $data[] = $row;
            });

            if (count($data) == 0) { //no data
                return false;
            }

            $used = [];
            $noUsed = [];
            foreach($data as $key=>$row) {
                if (preg_match("/.*(份|碗|包|瓶|罐|杯|個|个|顆|根|支|bag|pc|cup|pack)/i", $row->unit)) {
                    $used[] = $row->value;
                }else {
                    $noUsed[] = $row->value;
                }
            }

            if (count($used) > 0) {
                $resultValue = $this->calculate_median($used);
            }else if (count($noUsed) > 0){
                $resultValue = $this->calculate_median($noUsed);
            }else {
                $resultValue = -1;
            }

            if ($resultValue != -1) {
                $oCalorie = new Calorie();
                $oCalorie->name = $search;
                $oCalorie->value = $resultValue;
                $oCalorie->data = json_encode($data);
                $oCalorie->save();

                $calorie_id = $oCalorie->id;
            }

        }else {
            $calorie_id = $rowSearch->id;
        }


        if ($resultValue != -1) {
            $oDiet = Diet::with('member')->find($diet_id);
            if ($oDiet) {

                if ($isNotification) {

                    $toMember = $oDiet->member;
                    $toMember->notify(new MyDynamic(Member::find(31),'rated',$oDiet->id,$oDiet->calorie_id > 0 ? 1 : 0));

                }

                $oDiet->calorie_id = $calorie_id;
                $oDiet->save();

            }
        }


    }

    function calculate_median($inputArr) {

        asort($inputArr);

        $arr = array_values($inputArr);

        $count = count( $arr );
        $middleval = floor(($count - 1) / 2);
        if( $count % 2 ) {
            $median = $arr[ $middleval ];
        } else {
            $low = $arr[ $middleval ];
            $high = $arr[ $middleval + 1 ];
            $median = (( $low + $high ) / 2);
        }

        return round($median);
    }
}
