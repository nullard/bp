<?php

namespace App\Console\Commands;

use App\Diet;
use App\Mail\Template;
use App\Member;
use App\Notifications\FcmNotification;
use App\OrderPayment;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DietNoUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'diet:noUpload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '受測者24小時內未上傳圖片通知';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    var $hours = 24;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isProduction = config('app.env') == 'production' ? true : false;
        $subject = ($isProduction ? '[系統通知]' : '[TEST系統通知]').'受測者超過'.$this->hours.'小時未上傳飲食或體重';

        $now = Carbon::now()->format('Y-m-d H:i:s');
        $members = Member::where('status',1)->whereIn('id',function($query) use($now){
            $query->select('member_id')
                ->from(with(new OrderPayment())->getTable())
                ->where('sdate', '<=', $now)
                ->where('edate', '>=', $now)
                ->groupBy('member_id');
        })->get();

        $data = [];
        $weight_data = [];
        foreach($members as $member) {

//            $row = Diet::where('member_id',$member->id)->where('type','!=','weight')->latest()->first();
            $row = Diet::where('member_id',$member->id)->where(function($query) {
                $query->whereNull('type');
                $query->orWhere('type','!=','weight');
            })->latest()->first();
            $weight_row = Diet::where('member_id',$member->id)->where('type' ,'weight')->latest()->first();

            if ($row) {
                $rs = $this->output($member,$now,$row->created_at);
            }else {
                $rs = $this->output($member,$now,$member->created_at);
            }

            if ($weight_row) {
                $weight_rs = $this->output($member,$now,$weight_row->created_at,48);
            }else {
                $weight_rs = $this->output($member,$now,$member->created_at,48);
            }

            if (isset($rs)) {
                $data[] = $rs;
            }

            if (isset($weight_rs)) {
                $weight_data[] = $weight_rs;
            }



        }

        $final_content = '';

        if (count($data) > 0) {
            $content = "名單如下：\n";
            foreach($data as $row) {
                $content .= "## ".$row['name']." - ".$row['overTime']."未動作\n";

                \Notification::send(Member::find($row['id'])->first(),new FcmNotification($row['id'], '食物上傳通知', '超過一天沒見到您的食物照片了，快來上傳吧'));
            }
            $final_content .= $content;
        }


        if (count($weight_data) > 0) {

            if ($final_content != '') {
                $weight_content = "---------------------------------------\n";
            }else {
                $weight_content = '';
            }

            $weight_content .= "體重未上傳名單如下：\n";
            foreach($weight_data as $row) {
                $weight_content .= "## ".$row['name']." - ".$row['overTime']."未動作\n";

                \Notification::send(Member::find($row['id'])->first(),new FcmNotification($row['id'], '體重上傳通知', '超過一天沒見到您的體重照片了，快來上傳吧'));
            }

            $final_content .= $weight_content;
        }



        if ($final_content != '') {
            if ($isProduction) {
                Mail::to('nulla@nulla.com.tw')->send(new Template($subject,$final_content));
            }else {
                Mail::to('fm@nulla.com.tw')->send(new Template($subject,$final_content));
            }
        }

    }

    public function output($member,$now,$campare_time,$diff_hours=null)
    {
        $diff = Carbon::parse($now)->diffInHours(Carbon::parse($campare_time));

        $hours = $this->hours;
        if (isset($diff_hours)) {
            $hours = $diff_hours;
        }

        if ($diff >= $hours) {

            Carbon::setLocale('zh-tw');
            $data['id'] = $member->id;
            $data['name'] = isset($member->real_name) ? $member->real_name : $member->name;
            $data['overTime'] = Carbon::parse($campare_time)->diffForHumans(Carbon::parse($now));

            return $data;


        }

        return null;
    }
}
