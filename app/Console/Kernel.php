<?php

namespace App\Console;

use App\Console\Commands\DietCaloria;
use App\Console\Commands\DietNoRateCommand;
use App\Console\Commands\DietNoUpload;
use App\Console\Commands\GetCreditPeriodInfo;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DietNoRateCommand::class,
        DietNoUpload::class,
        GetCreditPeriodInfo::class,
        DietCaloria::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('diet:noRate')->dailyAt('16:00');
        $schedule->command('diet:noUpload')->dailyAt('16:00');

        $schedule->command('getCreditPeriod:info')->cron('0 6 7 1-11 *');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
