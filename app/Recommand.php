<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommand extends Model
{
    protected $fillable = [
        'name', 'type', 'status', 'qrcode', 'member_id'
    ];

    public static function getDataByQRCode($qrcode)
    {

        return self::where('qrcode',$qrcode)->first();

    }
}
