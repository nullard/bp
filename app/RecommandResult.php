<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecommandResult extends Model
{
    protected $fillable = [
        'recommand_id','recommand_date','member_count','member_pay_count'
    ];
}
