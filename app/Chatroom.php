<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\Relation;
use Auth;

Relation::morphMap([
    'members' => 'App\Member',
]);

class Chatroom extends Model
{
    protected $fillable = [
        'fromtable_type', 'fromtable_id'
    ];

    function fromtable() {
        return $this->morphTo();
    }

    function badge() {
        return $this->hasOne('\App\MsgRead','chatroom_id','id')->where('user_id',Auth::id());
    }

    /*
    function member() {

        return $this->hasOne('App\Member','id','ref_id')->where('type','user');

    }*/

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

}
