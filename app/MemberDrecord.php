<?php

namespace App;

use App\Notifications\MemberLineNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class MemberDrecord extends Model
{
    use Notifiable;

    protected $fillable = [
        'member_id', 'sdate','member_kg_target_id', 'member_cal_target_id'
    ];


    public function weight_diet()
    {
        return $this->belongsTo('App\Diet','weight_diet_id');
    }

    static public function getRecord($member_id,$sdate=null)
    {
        if (!isset($sdate)) {
            $sdate = Carbon::now()->format('Y-m-d');
        }

        $oRecord = self::where('member_id',$member_id)
            ->where('sdate',$sdate)
            ->first();
        if (!$oRecord) {
            $oPrevRecord = self::where('member_id',$member_id)
                ->where('sdate','<',$sdate)
                ->orderBy('sdate','desc')
                ->first();

            if ($oPrevRecord) {
                $oRecord = $oPrevRecord->replicate();
                $oRecord->cal = 0;
            }else {
                $oRecord = new self;
                $oRecord->cal = 0;
                $oRecord->target_cal = 1800;
            }

            $oRecord->member_id = $member_id;
            $oRecord->sdate = $sdate;

            $oRecord->save();
        }

        return $oRecord;
    }

    static public function updateCaloria($member_id,$sdate,$value,$social_id = null)
    {
        $oRecord = self::getRecord($member_id,$sdate);

        $oRecord->cal += $value;

        if ($oRecord->cal < 0 ) {
            $oRecord->cal = 0;
        }


        $oRecord->save();

        return $oRecord;
    }

    static public function updateBp($type,$member_id,$sdate,$sbp,$dbp,$diet_id=0)
    {

        $oRecord = self::getRecord($member_id,$sdate);

        if( $type == 'create' ) {
            $oRecord->sbp = $sbp;
            $oRecord->dbp = $dbp;
            $oRecord->weight_diet_id = $diet_id;
        }else {
            $oDiet = Diet::where('meal_time','like',"$sdate%")
                ->where('type','bp')
                ->orderBy('meal_time','desc')
                ->orderBy('id','desc')->first();


            if ($oDiet) {
                $oRecord->sbp = $oDiet->sbp;
                $oRecord->dbp = $oDiet->dbp;
                $oRecord->weight_diet_id = $oDiet->id;
            }else {
                $oRecord->sbp = 0;
                $oRecord->dbp = 0;
                $oRecord->weight_diet_id = 0;
            }

        }

        $oRecord->save();

        return $oRecord;

    }

}
