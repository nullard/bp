<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_CALLBACK_URL'),
    ],

    'facebook_20180612001' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_CALLBACK_URL_20180612001'),
    ],

    'line' => [
        'client_id' => env('LINE_CLIENT_ID'),
        'client_secret' => env('LINE_CLIENT_SECRET'),
        'redirect' => env('LINE_CALLBACK_URL'),
    ],

    'so88' => [
        'client_secret' => env('SO88_CLIENT_SECRET')
    ],

    'ecpay' => [
        'dev' => [
            'service_url' => env('DEBUG_ECPAY_SERVICE_URL'),
            'hashkey' => env('DEBUG_ECPAY_HASHKEY'),
            'hashiv' => env('DEBUG_ECPAY_HASHIV'),
            'merchantid' => env('DEBUG_ECPAY_MERCHANTID'),
            'invoice_service_url' => env('DEBUG_ECPAY_INVOICE_SERVICE_URL'),
            'invoice_hashkey' => env('DEBUG_ECPAY_INVOICE_HASHKEY'),
            'invoice_hashiv' => env('DEBUG_ECPAY_INVOICE_HASHIV'),
        ],
        'release' => [
            'service_url' => env('ECPAY_SERVICE_URL'),
            'hashkey' => env('ECPAY_HASHKEY'),
            'hashiv' => env('ECPAY_HASHIV'),
            'merchantid' => env('ECPAY_MERCHANTID'),
            'invoice_service_url' => env('ECPAY_INVOICE_SERVICE_URL'),
            'invoice_hashkey' => env('ECPAY_INVOICE_HASHKEY'),
            'invoice_hashiv' => env('ECPAY_INVOICE_HASHIV'),
        ]
    ]

];
