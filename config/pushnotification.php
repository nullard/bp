<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAYMA1N3s:APA91bGxreSQvcxZVFkpuhtnNVddDGNqaiVQNQsGrGlQA1IggNJXwRmPaV5wpHWAVawMAX6N4Weaukdd0Cj7IWG94L30Zv6Oa08ta3HVfW-vzIlziTz2e8Ki22gwwSojdoGDSiMDWCQa',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/' . (env('APP_ENV', 'local') == 'local' ? 'apns-dev-cert.pem' : 'apns-prod-cert.pem'),
//      'passPhrase' => '', //Optional
//      'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => true
  ]
];