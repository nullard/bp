<body>
您好:<br/><br/>

如果您有申請密碼變更作業，請儘速於三個小時內，點以下連結，進行密碼變更<br/><br/>

如果否，請忽略此信件<br/><br/>


<a href="{{ url('/change/password/'.$member->code.'/'.$member->forget_token) }}">{{ url('/change/password/'.$member->code.'/'.$member->forget_token) }}</a>

<br/>
<br/>
系統信件請勿直接回覆

<br/><br/><br/>
Thank you,<br/>
瘦88團隊



</body>