{{ csrf_field() }}
<div class="form-group">
    <label for="exampleInputName2">姓名</label>
    <input type="text" class="form-control" name="name" id="exampleInputName2" placeholder="姓名" value="{{ $appends['name'] or '' }}">
</div>
@if(Auth::user()->role == 'admin' || Auth::user()->role == 'counselor')
    <div class="form-group">
        <label for="exampleInputEmail2">付款</label>
        <select class="form-control" name="pay_status">
            <option value="" @if( !isset($appends['pay_status']) || $appends['pay_status'] == '') selected @endif>[請選擇]</option>
            <option value="1" @if( isset($appends['pay_status']) && $appends['pay_status'] == 1 ) selected @endif>已付款</option>
            <option value="0" @if( isset($appends['pay_status']) && $appends['pay_status'] == 0 ) selected @endif>未付款</option>
        </select>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail2">營養師</label>
        <select class="form-control" name="user_id">
            <option value="" @if( !isset($appends['user_id']) || $appends['user_id'] == '') selected @endif>[請選擇]</option>
            <option value="0" @if( isset($appends['user_id']) && $appends['user_id'] == '0' ) selected @endif>待指派</option>
            @foreach($nutritionists as $nutritionist)
                <option value="{{ $nutritionist->id }}" @if( isset($appends['user_id']) && $appends['user_id'] == $nutritionist->id ) selected @endif>{{ empty($nutritionist->real_name) ? $nutritionist->name : $nutritionist->real_name }}</option>
            @endforeach
        </select>
    </div>

@endif