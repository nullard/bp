@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0 for-mobile">
            <div class="panel panel-default">
                <div class="panel-heading">會員管理</div>
                <div class="panel-body">
                    <!--
                    <div class="hidden-xs hidden-sm" style="padding: 10px 0;">
                        <form class="form-inline" action="{{ route('admin.members') }}" method="post">
                            @include('admin.members.search')
                            <button type="submit" class="btn btn-default">搜尋</button>
                        </form>
                    </div>

                    <div class="visible-xs visible-sm" style="padding: 10px 0;">
                        <a href="#openModal" class="remove-a-tag-default"><button type="submit" class="btn btn-default">搜尋條件</button></a>
                        <div class="modalDialog" id="openModal">
                            <div class="modalContent">
                                <a href="#close" title="Close" class="close"><i class="material-icons">close</i></a>
                                <h4>搜尋條件</h4>
                                <hr>
                                <form class="form-inline" action="{{ route('admin.members') }}" method="post">
                                    @include('admin.members.search')
                                    <button type="submit" class="btn btn-default" style="width:100%">搜尋</button>
                                </form>
                            </div>
                        </div>
                    </div>!-->


                    <table class="table table-bordered table-responsive">
                        <tr>
                            <td class="text-center hidden-xs hidden-sm" width="60">ID</td>
                            <td class="text-center">姓名</td>
                            <td class="text-center" width="50">性別</td>
                            <td class="text-center" width="50">年齡</td>
                            <td class="text-center" width="50">飲食</td>
                            <td class="text-center" width="110">BH·BW·BMI</td>
                            <td class="text-center hidden-sm hidden-xs" width="110">專家</td>
                            <td class="text-center hidden-sm hidden-xs" width="110">推薦人</td>
                        </tr>
                        @inject('MemberPresenter','App\Presenters\MemberPresenter')
                        @foreach( $data as $item)
                            @php
                                $item = (object) $item;
                                $bmi = $MemberPresenter->getBMI($item->tall,$item->weight);
                                $isDisable = $item->status == 0 ? true : false;
                            @endphp
                            <tr class="active">
                                <td class="text-center hidden-xs hidden-sm {{ $isDisable ? 'aaa' : ''}}">{{ $item->id }}</td>
                                <td class="{{ $isDisable ? 'aaa' : ''}}">{{ $item->level == 'nutritionist' ? '[營] ' : '' }}{{ isset($item->real_name) ? $item->real_name : $item->name }}</td>
                                <td class="text-center {{ $isDisable ? 'aaa' : ''}}">{{ $MemberPresenter->getSex($item->sex) }}</td>
                                <td class="text-center {{ $isDisable ? 'aaa' : ''}}">{{ $MemberPresenter->getAge($item->birth) }}</td>
                                <td class="text-center {{ $isDisable ? 'aaa' : ''}}">{{ $MemberPresenter->getEatType($item->vegetarian) }}</td>
                                <td class="{{ $isDisable ? 'aaa' : ''}}">{{ $item->tall }}·{{ $item->weight }}·{{ $bmi }}</td>
                                <td class="hidden-sm hidden-xs {{ $isDisable ? 'aaa' : ''}}">{{ $item->nutritionist }}</td>
                                <td class="hidden-sm hidden-xs {{ $isDisable ? 'aaa' : ''}}">{{ $item->recommander }}</td>
                            </tr>
                            @php
                                $warnPayment = !$MemberPresenter->checkPayment($item->id);
                            @endphp
                            @if($item->status == 1 && ($warnPayment))
                            <tr>
                                <td colspan="8" style="color:red;">
                                        @if($warnPayment && $item->level == 'patient')
                                            <i class="material-icons" style="font-size:16px;vertical-align: text-bottom">warning</i> 尚未付款
                                        @endif
                                        @if($bmi == 0)
                                            <i class="material-icons" style="font-size:16px;vertical-align: text-bottom">warning</i> 身高體重未填寫
                                        @endif
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <td colspan="8" class="text-right">
                                    <span style="color: #F00;">
                                        <b>{{ $isDisable ? '停用' : '' }}</b>
                                    </span>
                                    <button type="submit" class="btn btn-success btn-sm" onclick="location.href='{{ route('admin.members.edit',['id'=>$item->id]) }}'">個資編輯</button>
                                    <button type="submit" class="btn btn-primary btn-sm" onclick="location.href='{{ route('admin.members.show',['id'=>$item->id]) }}'">查看資料</button>
                                </td>
                            </tr>
                        @endforeach
                        @if(count($data) == 0)
                            <tr>
                                <td colspan="8" class="text-center">無資料</td>
                            </tr>
                        @endif

                    </table>

                    <div style="text-align: center">
                        {{ $rows->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
