@extends('admin.layouts.app')

@section('content')
<div class="container member-show-page">
    <div class="row">

        <div class="col-md-6 col-md-offset-3 for-mobile"> <!-- col-md-offset-3 !-->

            @inject('MemberPresenter','App\Presenters\MemberPresenter')

            @if(Auth::user()->role == 'admin' || Auth::user()->role == 'counselor')
            <div class="panel panel-default">
                <div class="panel-heading">會員 #{{ $row->id }}</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="get" action="{{ route('admin.members.edit',['id'=>$row->id]) }}" onsubmit="return false">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $row->id }}">
                        <table class="table table-bordered">
                            <tr>
                                <td class="text-right" width="100" class="">名稱</td>
                                <td>{{ $row->real_name }} ({{ $row->name }})</td>
                            </tr>
                            <tr>
                                <td class="text-right">性別</td>
                                <td>{{ $MemberPresenter->getSex($row->sex) }}</td>
                            </tr>
                            <tr>
                                <td class="text-right">出生</td>
                                <td>{{ $row->birth }} ({{ $MemberPresenter->getAge($row->birth) }})</td>
                            </tr>
                            @if ($row->level == 'patient')
                            <tr>
                                <td class="text-right">營養師</td>
                                <td>{{ $MemberPresenter->getNutritionist($row->parent_id) }}</td>
                            </tr>
                            <tr>
                                <td class="text-right">等級</td>
                                <td>{{ $MemberPresenter->getLevel($row->level) }}</td>
                            </tr>
                            <tr>
                                <td class="text-right">月費</td>
                                <td>{{ $row->pay }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td class="text-right">吃素</td>
                                <td>{{ $row->vegetarian == 1 ? 'Y' : 'N' }}</td>
                            </tr>

                            <tr>
                                <td class="text-right">身高 / 體重</td>
                                <td>{{ $row->tall }} cm / {{ $row->weight }} kg</td>
                            </tr>

                            @php
                            $bmi = $MemberPresenter->getBMI($row->tall,$row->weight);
                            @endphp
                            <tr>
                                <td class="text-right">BMI</td>
                                <td>{{ $bmi }}</td>
                            </tr>
                            <tr>
                                <td class="text-right"></td>
                                <td>{{ $MemberPresenter->getBMILevel($bmi) }}</td>
                            </tr>

                            <tr>
                                <td class="text-right">狀態</td>
                                <td>@if($row->status == 1) 啟用 @endif</td>
                            </tr>
                        </table>

                        <hr/>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
                                <button type="button" class="btn btn-default" onclick="location.href='{{ route('admin.members.destroy',["id"=>$row->id]) }}'">註銷帳號</button>
                                <button type="button" class="btn btn-default" onclick="location.href='{{ route('admin.members') }}'">返回</button>
                                <button type="button" class="btn btn-default" onclick="location.href='{{ route('admin.members.edit',['id'=>$row->id]) }}'">編輯</button>
                            </div>
                        </div>

                        <div>
                            <table class="table table-bordered">
                                <tr>
                                    <td>付款日期</td>
                                    <td>生效起訖</td>
                                    <td>付款狀態</td>
                                </tr>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->pay_date }}</td>
                                        <td>{{ $payment->sdate }} - {{ $payment->edate }}</td>
                                        <td>{{ $payment->status == 1 ? '已付' : '未付' }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </form>

                </div>
            </div>
            @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $row->real_name }}·{{ $MemberPresenter->getSex($row->sex) }}·{{ $MemberPresenter->getAge($row->birth) }}·{{ $MemberPresenter->getEatType($row->vegetarian) }} / BH:{{ $row->tall }} BW:{{ $row->weight }} BMI:{{ $MemberPresenter->getBMI($row->tall,$row->weight) }}
                </div>
            </div>

            @endif

        </div>


    </div>
</div>
@endsection
