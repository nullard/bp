@extends('admin.layouts.app')

@section('content')
<div class="container" id="test-id">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 for-mobile">
            <div class="panel panel-default">
                <div class="panel-heading">會員 #{{ $row->id }}</div>
                @inject('MemberPresenter','App\Presenters\MemberPresenter')
                <div class="panel-body">

                    <form class="form-horizontal" method="post" action="{{ route('admin.members.store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $row->id }}">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">暱稱</label>
                            <div class="col-sm-10">
                                <label for="" class="control-label">{{ $row->name }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">姓名</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="姓名" value="{{ $row->real_name }}" name="real_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">性別</label>
                            <div class="col-sm-10">
                                @php
                                    $sexs = ['-2','0','1'];
                                @endphp
                                <select class="form-control" name="sex">
                                    @foreach($sexs as $sex)
                                        <option value="{{ $sex }}" @if($row->sex == $sex) selected @endif>{{ $MemberPresenter->getSex($sex) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">出生</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="出生" value="{{ $row->birth }}" name="birth">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">角色</label>
                            <div class="col-sm-10">
                                @php
                                    $levels = ['patient','nutritionist'];
                                @endphp
                                <select class="form-control" name="level">
                                    @foreach($levels as $level)
                                        <option value="{{ $level }}" @if($row->level == $level) selected @endif>{{ $MemberPresenter->getLevel($level) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">營養師</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="parent_id">
                                    <option value="0">[請選擇]</option>
                                    @foreach($MemberPresenter->getNutritionistList() as $nutritionist)
                                        <option value="{{ $nutritionist->id }}" @if($row->parent_id == $nutritionist->id) selected @endif>{{ $nutritionist->real_name }}({{ $nutritionist->name }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">月費</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" placeholder="月費" value="{{ $row->pay }}" name="pay">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">吃素</label>
                            <div class="col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="vegetarian" value="1" @if($row->vegetarian == 1) checked @endif> Y
                                    </label>
                                </div>
                            </div>
                        </div>

                        <hr/>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">身高(cm)</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" placeholder="身高" value="{{ $row->tall }}" name="tall">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">體重(kg)</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" placeholder="體重" value="{{ $row->weight }}" name="weight">
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="status" value="1" @if($row->status == 1) checked @endif> 啟用
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
                                <button type="button" class="btn btn-default" onclick="history.back()">返回</button>
                                <button type="submit" class="btn btn-default">儲存</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection