@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 for-mobile">
            <div class="panel panel-default">
                <div class="panel-heading">推薦設定</div>

                <div class="panel-body">
                    <div style="text-align: right;padding-bottom: 5px;">
                        <button type="button" class="btn btn-default btn-sm" onclick="location.href='{{ route('admin.recommand.edit') }}'">新增</button>
                    </div>

                    @inject('RecommandPresenter','App\Presenters\RecommandPresenter')
                    <table class="table table-bordered">
                        <tr>
                            <td width="100">類別</td>
                            <td width="100">姓名</td>
                            <td width="100">會員ID</td>
                            <td width="100">Code</td>
                            <td>啟用狀態</td>
                            <td width="100">管理</td>
                            <td width="100">顯示</td>
                        </tr>
                        @foreach( $rows as $item)
                        <tr>
                            <td>{{ $RecommandPresenter->getType($item->type) }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ empty($item->member_id) ? '' : $item->member_id }}</td>
                            <td>{{ $item->qrcode }}</td>
                            <td>{{ $item->status == 1 ? 'Y' : 'N' }}</td>
                            <td>
                                <button type="submit" class="btn btn-success btn-sm" onclick="location.href='{{ route('admin.recommand.edit',['id'=>$item->id]) }}'">
                                    編輯
                                </button>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-primary uccess btn-sm" onclick="location.href='{{ route('admin.recommand.show',['id'=>$item->id]) }}'">
                                    顯示
                                </button>
                            </td>
                        </tr>
                        @endforeach

                    </table>

                    <div style="text-align: center">
                        {{ $rows->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
