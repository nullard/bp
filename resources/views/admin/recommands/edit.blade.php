@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">推薦設定</div>

                <div class="panel-body">

                    <form class="form-horizontal" method="post" action="{{ route('admin.recommand.store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $row->id }}">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">類別</label>
                            <div class="col-sm-10">
                                <select name="type" class="form-control">
                                    <option value="dr" @if($row->type == 'dr') selected @endif>醫生</option>
                                    <option value="other" @if($row->type == 'other') selected @endif>其他</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">姓名</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="姓名" value="{{ $row->name }}" name="name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">會員ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="會員ID" value="{{ $row->member_id }}" name="member_id">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="代碼" value="{{ $row->qrcode }}" name="qrcode">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="status" value="1" @if($row->status == 1) checked @endif> 啟用
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
                                <button type="button" class="btn btn-default" onclick="history.back()">返回</button>
                                <button type="submit" class="btn btn-default">儲存</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
