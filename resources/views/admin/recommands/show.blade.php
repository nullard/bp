@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">推薦設定 #{{ $row->id }}</div>
                @inject('RecommandPresenter','App\Presenters\RecommandPresenter')
                <div class="panel-body">

                    <table class="table table-bordered">
                        <tr>
                            <td class="text-right" width="150" class="">類別</td>
                            <td>{{ $RecommandPresenter->getType($row->type) }}</td>
                        </tr>
                        <tr>
                            <td class="text-right" width="150" class="">名稱</td>
                            <td>{{ $row->name }}</td>
                        </tr>
                        <tr>
                            <td class="text-right" width="150" class="">會員ID</td>
                            <td>{{ empty($row->member_id) ? '':$row->member_id }}</td>
                        </tr>
                        <tr>
                            <td class="text-right" width="150" class="">Code</td>
                            <td>{{ $row->qrcode }}</td>
                        </tr>
                    </table>


                    <hr/>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
                            <button type="button" class="btn btn-default" onclick="location.href='{{ route('admin.recommand') }}'">返回</button>
                            <button type="button" class="btn btn-default" onclick="location.href='{{ route('admin.recommand.edit',['id'=>$row->id]) }}'">編輯</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


