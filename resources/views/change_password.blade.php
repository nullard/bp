@extends('layouts.app')

<style>
    body {
        font-size: 16px;
    }
    #header {
        width: 100%;
        text-align: center;
        height: 50px;
        border-bottom: 1px solid #DCDCDC;
        font-size: 28px;
        line-height: 50px;
        color: #72B5C4;
    }
    #content {
        max-width: 600px;
        margin: 0 auto;
    }
    .column {
        margin-top: 20px;
        padding: 0 30px;
    }
    input[type=password] {
        width: 100%;
        height: 50px;
        font-size: 20px;
        border: 1px solid #DBDBDB;
        border-radius: 2px;
        outline: none;
        padding: 0px 10px;
    }
    input[type=button] {
        width: 100%;
        height: 50px;
        font-size: 20px;
        color: #FFF;
        font-weight: bold;
        background: #488DFC;
        border: 1px solid #488DFC;
        border-radius: 6px;
    }
</style>

<div id="header">
    瘦88
</div>

<div id="content">
    <form id="submitForm" action="{{ url('/change/password') }}" method="post">
        {{ csrf_field() }}

        @if(!isset($msg))
            <div class="column">
                如果有申請密碼變更，請儘速完成變更作業
            </div>

            <input type="hidden" name="token" value="{{ $oMember->forget_token }}">


            <div class="column">
                <label for="">帳號</label>
                <div>
                    {{ $oMember->email }}
                </div>
            </div>

            <div class="column">
                <label for="">密碼(至少8個字元以上)</label>
                <div>
                    <input id="password" type="password" name="password" placeholder="密碼">
                </div>
            </div>

            <div class="column">
                <label for="">確認密碼</label>
                <div>
                    <input id="re-password" type="password" name="re-password" placeholder="確認密碼">
                </div>
            </div>

            <div class="column">
                <input id="send" type="button" value="密碼變更" />
            </div>
        @else
            <br/>
            <br/>
            <div style="text-align: center">
                <span style="color:red">{{ $msg }}</span>
            </div>

        @endif

    </form>
</div>

<script>
    @if(Session::has('status'))
            alert("{{ Session::get('status') }}")
            setTimeout(()=> {
                location.href = "/";
            },500)
    @endif
    function validation() {

        var password = document.getElementById('password').value;
        var re_password = document.getElementById('re-password').value;

        if (password == "") {
            alert("密碼為必填");
            return false
        }

        if (password.length < 8){
            alert("密碼至少8位元以上");
            return false
        }

        if (password != re_password) {
            alert("兩次密碼不相符");
            return false
        }


        return true;

    }

    function submit() {

        if ( validation() ) {
            document.getElementById("submitForm").submit();
            return false;
        }

    }


    document.getElementById("send").addEventListener("click", function(e) {
        submit();
    });

</script>

