<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0"><!--, viewport-fit=cover!-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">

    <link rel="stylesheet" href="{{ mix('css/vue-material/all.css') }}">
    <link rel="stylesheet" href="{{ mix('css/framework7-icon/framework7-icons.css') }}">

    <!-- vendors/exif-js/exif-js.js !-->
    <script src="{{ mix('js/plugins/exif.js') }}"></script>
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <script>

        window.fbAsyncInit = function() {
            FB.init({
                appId            : '505434933267923',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
            /*
            FB.getLoginStatus( response => {
                console.log('res', response)
            })*/
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/zh_TW/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="app" @scroll.native="handleScroll">

        <keep-alive>
            <router-view v-if="$route.meta.keepAlive" :key='$route.fullPath'></router-view>
        </keep-alive>

        <router-view v-if="!$route.meta.keepAlive"  :key='$route.fullPath'></router-view>


        <!--
        <transition name="fade" mode="out-in">
            <router-view></router-view>
        </transition>!-->


        <so88-alert ref="alert"></so88-alert>
        <so88-confirm ref="confirm"></so88-confirm>
        <so88-prompt ref="prompt"></so88-prompt>
    </div>


    <script>
        var deviceType = '';
        var deviceToken = '';
    </script>
    <!-- Scripts -->
    <script src="/js/manifest.js"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript">

        $(function() {
            /*
            $("body").prepend("<br/><h1> 000000 </h1>");

            let url = new URL(window.location.href);
            let m = url.searchParams.get("m");*/
        })

        function setDeviceToken(type,token) {
            deviceType = type
            deviceToken = token
        }

        function setLogin(access_token,code) {

            $.ajax({
                url : "/api/mobile/setLogin",
                method: "POST",
                data: {access_token: access_token, code: code},
                dataType: "json",
                success: function(res) {

                    if (res.status == 200) {
                        app.__vue__.setUser(res.token);
                    }else {
                        app.__vue__.gotoLoginPage();
                    }

                },
                async: false
            });


            return "any(iOS, android) device login";

        }
        /*
        function setLogin(access_token,code) {


            axios.post('/api/mobile/setLogin',{access_token: access_token, code: code}).then(function(res) {


                if (res.data.status == 200) {

                    let loginCount = 0;
                    if (sessionStorage.getItem("loginCount")) {
                        loginCount = parseInt(sessionStorage.getItem("loginCount"));
                        if (loginCount > 3) {
                            app.__vue__.gotoLoginPage();
                            return;
                        }
                    }else {
                        loginCount = 0;
                    }

                    let old_id = app.__vue__.$store.getters.user.id;
                    app.__vue__.setUser(res.data.token);

                    if (old_id == '') {

                        loginCount = loginCount + 1;
                        sessionStorage.setItem('loginCount', loginCount);

                        setTimeout(function() {
                            app.__vue__.$router.go(0);
                        },1200)

                        return "Go Go ~~~";

                    }else {
                        sessionStorage.removeItem('loginCount');
                    }

                }else {

                    app.__vue__.gotoLoginPage();
                    return;

                }
            })
        }
        */
    </script>


</body>
</html>
