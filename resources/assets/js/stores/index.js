import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        queryTime:0,
        tabname:null,
        user: {
            notice_badge: 0,
            role: 'patient',
            name: '',
            photo: '',
            id: 0,
            desp: '',
            tall: '',
            is_admin: 0,
            god_vw: 0,
            recommand: ''
        },
        createImage: {
            isEditLoad: false, //編輯時，是否已載入資料
            isRated: false,
            unique: 0,
            name: '',
            type: 'caloria',
            image:{
                isZoom: false,
                points:{startX:0,startY:0},
                scale:0,
                orientation:1,
                image: null,//url
                origin: null,
                crop: null,
            },
            isDisabled: false,
            desp:'',
            value:'',
            meal_time:'',
            share_type: null,
            isMoon: false
        },
        authStatus: {
            isLoading: false,
            isLogin: null
        },
        viewUser: {
            unique: 0
        }
    },
    getters: {
        createImage: state=> {
            return state.createImage;
        },
        user: state=> {
            return state.user;
        },
        tabname: state=> {
            return state.tabname;
        },
        queryTime: state=> {
            return state.queryTime;
        },
        authStatus:  state => {
            return state.authStatus;
        },
        viewUser: state=> {
            return state.viewUser;
        }
    },
    mutations: {
        updateCreateImage (state,payload) {
            state.createImage = payload;
        },
        clearCreateImage(state) {
            state.createImage = {
                isEditLoad: false,
                isRated: false,
                unique: 0,
                name: '',
                type: 'caloria',
                image: {
                    isZoom: false,
                    points:{startX:0,startY:0},
                    scale:0,
                    orientation:1,
                    image: null,
                    origin: null,
                    crop: null,
                    isDisabled: false,
                },

                desp: '',
                value: '',
                meal_time: '',
                share_type: null
            }
        },
        clearUser(state) {
            state.user = {
                id: 0
            }
        },
        updateUser (state,payload) {
            state.user = payload;
        },
        updateTabname(state,payload) {
            state.tabname = payload;
        },
        updateQueryTime(state) {
            console.log('time load');
            state.queryTime = new Date().getTime();
        },
        updateAuthStatus(state,payload) {
            state.authStatus = payload;
        },
        updateViewUser(state, payload) {
            state.viewUser = payload;
        }
    },
    actions: {
        updateCreateImage ({ state, commit },payload) {
            commit('updateCreateImage',payload)
        },
        clearCreateImage({ state, commit }) {
            commit('clearCreateImage')
        },
        clearUser({state,commit}) {
          commit('clearUser');
        },
        updateUser({ state, commit },payload) {
            console.log("update user");
            commit('updateUser',payload)
        },
        updateTabname({ state, commit },payload) {
            commit('updateTabname',payload)
        },
        updateQueryTime({ state, commit }) {
            commit('updateQueryTime')
        },
        updateAuthStatus({ state, commit }, payload) {
            commit('updateAuthStatus',payload)
        },
        logoutAuthStatus({ state, commit }) {
            commit('updateAuthStatus',{
                isLoading: false,
                isLogin: null
            })
        },
        updateViewUser({ state, commit }, payload) {
            commit('updateViewUser',payload)
        }
    }
})