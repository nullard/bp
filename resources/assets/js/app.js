
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// 引入 vuex
import Vuex from 'vuex'
Vue.use(Vuex)

import VTooltip from 'v-tooltip'
Vue.use(VTooltip)

// import VueRouter from 'vue-router';

//https://github.com/michalsnik/vue-content-placeholders
import VueContentPlaceholders from 'vue-content-placeholders'
Vue.use(VueContentPlaceholders)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('navbar', require('./components/Navbar.vue'));
Vue.component('navbar-title', require('./components/NavbarTitle.vue'));
Vue.component('tabbar', require('./components/Tabbar.vue'));
Vue.component('card-share', require('./components/CardShare.vue'));
Vue.component('comment', require('./components/Comment.vue'));

//search bar
Vue.component('search-bar', require('./components/SearchBar.vue'));
Vue.component('user-item', require('./components/UserItems.vue'));

Vue.component('profile-title',require('./components/ProfileTitle'));

Vue.component('so88-alert',require('./components/Alert.vue'));
Vue.component('so88-confirm',require('./components/Confirm.vue'));
Vue.component('so88-prompt',require('./components/Prompt.vue'));

window.moment = require('moment');
require('moment/locale/zh-tw');
moment.locale('zh-tw');



import { createRouter } from './routers/router'
import store from './stores/index'
const router = createRouter();




import mixin from './mixins/mixin.js'
Vue.mixin(mixin);

//https://vuematerial.io/getting-started
import VueMaterial from 'vue-material';
Vue.use(VueMaterial)


const app = new Vue({
    mounted() {
        let _this = this
        setInterval(function() {
             _this.$store.dispatch('updateQueryTime');
        },1000 * 60 * 3)
    },
    router,
    store,
    el: '#app'
});
