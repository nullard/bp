var EXIF = require('exif-js');
import dataURLtoBlob from 'blueimp-canvas-to-blob'
var MobileDetect = require('mobile-detect');
var md = new MobileDetect(window.navigator.userAgent);

export default {
    mounted(){
    },
    data() {
      return {
          authStatus: {
              sLoading: false,
              isLogin: null
          }
      }
    },
    /*beforeRouteLeave(to,from,next){
        next();
    },*/
    methods: {
        auth(callback,fail) {

            let _this = this;

            _this.authStatus = _this.$store.getters.authStatus;

            if (!_this.isLoading) {
                _this.authStatus.isLoading = true;

                _this.$store.dispatch("updateAuthStatus",_this.authStatus);

                if (_this.authStatus.isLogin === null) {

                    console.log('login auth');
                    axios.get('/api/login/verify')
                        .then(function (response) {

                            if (response.data.status == 200) {

                                _this.authStatus.isLogin = true;

                                _this.setUser(response.data.token)

                                if (callback) {
                                    callback();
                                }

                            }else {
                                _this.authStatus.isLogin = false;
                                if(fail) {
                                    fail();
                                }else {
                                    _this.$router.replace({name:'login'})
                                }
                            }

                            _this.clearIsLogin();

                            _this.authStatus.isLoading = false;
                        })
                        .catch(function (error) {
                            if (error.response && error.response.status === 400) {
                                this.errors.setMessages(error.response.data.messages);
                            }
                            _this.authStatus.isLoading = false;
                        }.bind(this));

                    _this.$store.dispatch("updateAuthStatus",_this.authStatus);

                }else {
                    _this.authStatus.isLoading = false;
                    if (_this.authStatus.isLogin) {
                        if (callback) {
                            callback();
                        }
                    }else {
                        if(fail) {
                            fail();
                        }else {
                            _this.$router.replace({name:'login'})
                        }
                    }
                    _this.$store.dispatch("updateAuthStatus",_this.authStatus);
                }
            }

        },

        setUser(jwtToken) {
            let _this = this;
            let data = _this.parseJwt(jwtToken);

            let user = {
                notice_badge: data.notice_badge,
                role: data.role,
                name: data.name,
                photo: data.photo,
                id: data.id,
                desp: data.desp,
                tall: data.tall,
                weight: data.weight,
                post_share_type: data.post_share_type || 'expert',
                is_admin: data.is_admin,
                god_vw: data.god_vw,
                recommand: data.recommand
            };


            if(_this.$store.getters.user.notice_badge != data.notice_badge) {
                _this.$store.dispatch("updateQueryTime");
            }

            _this.$store.dispatch("updateUser",user);
        },

        //重新認證身份
        clearIsLogin() {
            let _this = this;
            setTimeout(function() {
                console.log('clear login flag');
                _this.authStatus = _this.$store.getters.authStatus;
                let autoCheck = _this.authStatus.isLogin;
                _this.authStatus.isLogin = null;

                if (autoCheck) {
                    _this.auth();
                }
                _this.$store.dispatch("updateAuthStatus",_this.authStatus);

            },30 * 60 * 1000);
        },
        xhrError(error,_this) {
            if (error.response && error.response.status === 401) {
                _this.$root.$refs.confirm.showAuth();
            }
        },
        parseJwt (token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse(window.atob(base64));
        },
        historyBack: function(argumentOne, argumentTwo, event) {
            this.$router.go(-1);
        },
        resizeSquareImage: function(file, width, height, callback) {

            let reader = new FileReader();
            reader.onloadend = function() {

                var sourceImage = new Image();
                sourceImage.src = reader.result;

                sourceImage.onload = (function () {

                    let canvas = document.createElement("canvas");
                    let ctx = canvas.getContext("2d");

                    canvas.width = width;
                    canvas.height = height;

                    let imgHeight = height;
                    let imgWidth = width;
                    let newImgSize = {width:sourceImage.width,height:sourceImage.height};
                    let isRotate = false;


                    //圖片旋轉
                    var that = this;
                    EXIF.getData(sourceImage, function(){
                        var orientation = EXIF.getTag(that, 'Orientation');

                        if(orientation == 6 || orientation == 8|| orientation == 3)
                        {
                            var rotateAngle = 0;

                            switch(orientation){
                                case 3:
                                    rotateAngle = 180;
                                    break;
                                case 6:
                                    rotateAngle = 90;
                                    canvas.width = imgHeight;
                                    canvas.height = imgWidth;
                                    break;
                                case 8:
                                    rotateAngle = -90;
                                    canvas.width = imgHeight;
                                    canvas.height = imgWidth;
                                    break;
                            }

                            var x = canvas.width / 2;
                            var y = canvas.height / 2;

                            ctx.translate(x, y);
                            ctx.rotate(rotateAngle*Math.PI/180);

                            isRotate = true;
                            //ctx.drawImage(sourceImage, (-imgWidth / 2), (-imgHeight / 2), imgWidth, imgHeight);
                        }
                        else
                        {
                            //ctx.drawImage(sourceImage, 0, 0, imgWidth, imgHeight);
                        }
                    });

                    if (newImgSize.width == newImgSize.height) {
                        if (isRotate) {
                            ctx.drawImage(sourceImage, (-imgWidth / 2), (-imgHeight / 2), width, height);
                        }else {
                            ctx.drawImage(sourceImage, 0, 0, width, height);
                        }

                    } else {
                        let minVal = Math.min(newImgSize.width, newImgSize.height);
                        if (newImgSize.width > newImgSize.height) {
                            if (isRotate) {
                                ctx.drawImage(sourceImage, (newImgSize.width - minVal) / 2, 0, minVal, minVal, (-imgWidth / 2), (-imgHeight / 2), width, height);
                            }else {
                                ctx.drawImage(sourceImage, (newImgSize.width - minVal) / 2, 0, minVal, minVal, 0, 0, width, height);
                            }

                        } else {
                            if (isRotate) {
                                ctx.drawImage(sourceImage, 0, (newImgSize.height - minVal) / 2, minVal, minVal, (-imgWidth / 2), (-imgHeight / 2), width, height);
                            }else {
                                ctx.drawImage(sourceImage, 0, (newImgSize.height - minVal) / 2, minVal, minVal, 0, 0, width, height);
                            }

                        }
                    }

                    let base64File = canvas.toDataURL("image/jpeg")

                    canvas.toBlob(function(blob){
                        let blobUrl = URL.createObjectURL(blob);
                        //console.log(blobUrl);
                        callback(blobUrl,base64File);

                    }, "image/jpeg", 1);
                });
            }

            reader.readAsDataURL(file);


        },
        methodThree: function(argumentOne, argumentTwo, event) {
            // code goes here
        },
        getDeviceToken: function() { //webview 橋接 無使用
            try {
                window.webkit.messageHandlers.getDeviceToken.postMessage('getDeviceToken')
            } catch(err) {
                console.log("not function");
            }
        },
        gotoLoginPage: function() {

            sessionStorage.removeItem('loginCount');

            this.$store.dispatch("clearUser");

            try {
                if ( md.match('iPhone|iPad|iPod') ) {
                    window.webkit.messageHandlers.gotoLoginPage.postMessage('gotoLoginPage2')
                }else {
                    so88Android.gotoLoginPage()
                }
            } catch(err) {
                console.log("not function");
            }
        },
        isNativeApp: function(sucess=null, fail=null) {

            try {

                if (md.match('iPhone|iPad|iPod')) {
                    window.webkit.messageHandlers.isNativeApp.postMessage('isNativeApp')
                }else {
                    so88Android.isNativeApp()
                }

                if (sucess) {
                    sucess()
                }

            } catch(err) {
                console.log("not function");
                if (fail) {
                    fail()
                }
            }

        },
        fbShareDialog: function(data=null,sucess=null,fail=null) {

            try {

                let passData = data;

                if (data != 'check') {
                    passData = JSON.stringify(data)
                }

                if (md.match('iPhone|iPad|iPod')) {
                    window.webkit.messageHandlers.fbShareDialog.postMessage(passData)
                }else {
                    so88Android.fbShareDialog(passData)
                }

                if (sucess) {
                    sucess()
                }

            } catch(err) {

                if (data == 'check') {
                    if (md.userAgent() === null) {
                        if (fail) {
                            fail()
                        }
                    }else {
                        if (sucess) {
                            sucess()
                        }
                    }
                }else {
                    if (fail) {
                        fail()
                    }
                }

            }

        },
        hello: function() {
          console.log("Hello World");
          return "hello";
        },
        //tab
        handleCreateImage(type,isEdit=0,updateCreateImage=null) {

            if (updateCreateImage) {
                updateCreateImage()
            }

            let _this = this;
            this.auth(function() {
                if (isEdit == 0) {
                    _this.$store.dispatch("clearCreateImage");
                }

                let filesToUpload = document.getElementById('take_picture').files;
                let file = filesToUpload[0];

                document.getElementById('take_picture').value = ''

                if (file.type.match('image.*')) {

                    _this.resize(file,function(file) {
                        let objectURL = file;

                        let img = _this.$store.getters.createImage;
                        img.image.orientation = 1;
                        _this.$store.dispatch("updateCreateImage", img);


                        _this.$router.push({ name: 'create', query: { image: objectURL, type: type }})


                    })

                }else {
                    alert("檔案格式不正確");
                }
            })


        },
        base64ToArrayBuffer (base64) {
            base64 = base64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
            var binaryString = atob(base64);
            var len = binaryString.length;
            var bytes = new Uint8Array(len);
            for (var i = 0; i < len; i++) {
                bytes[i] = binaryString.charCodeAt(i);
            }
            return bytes.buffer;
        },
        resize(file,callback) {
            let _this = this;
            var reader = new FileReader();
            reader.onloadend = function() {

                var tempImg = new Image();
                tempImg.src = reader.result;

                var exif = EXIF.readFromBinaryFile(_this.base64ToArrayBuffer(reader.result));
                var orientation = exif.Orientation;

                tempImg.onload = function() {

                    var MAX_WIDTH = 1024;
                    var MAX_HEIGHT = 1024;
                    var tempW = tempImg.width;
                    var tempH = tempImg.height;
                    if (tempW > tempH) {
                        if (tempW > MAX_WIDTH) {
                            tempH *= MAX_WIDTH / tempW;
                            tempW = MAX_WIDTH;
                        }
                    } else {
                        if (tempH > MAX_HEIGHT) {
                            tempW *= MAX_HEIGHT / tempH;
                            tempH = MAX_HEIGHT;
                        }
                    }

                    let canvas = document.createElement('canvas');
                    let ctx = canvas.getContext("2d");

                    canvas.width = tempW;
                    canvas.height = tempH;

                    ctx.drawImage(this, 0, 0, tempW, tempH);
                    var dataURL = canvas.toDataURL("image/jpeg",1);

                    _this.rotateOrientation(orientation,dataURL,callback)

                }

            }
            reader.readAsDataURL(file);
        },
        rotateOrientation(orientation,file,callback) {
            let _this = this;

            var img = new Image()
            img.onload = function() {

                let width = img.width
                let height = img.height
                let canvas = document.createElement('canvas');
                let ctx = canvas.getContext("2d");

                if (4 < orientation && orientation < 9) {
                    canvas.width = height;
                    canvas.height = width;
                } else {
                    canvas.width = width;
                    canvas.height = height;
                }

                // transform context before drawing image
                switch (orientation) {
                    case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
                    case 3: ctx.transform(-1, 0, 0, -1, width, height ); break;
                    case 4: ctx.transform(1, 0, 0, -1, 0, height ); break;
                    case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
                    case 6: ctx.transform(0, 1, -1, 0, height , 0); break;
                    case 7: ctx.transform(0, -1, -1, 0, height , width); break;
                    case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
                    default: break;
                }

                ctx.drawImage(this, 0, 0);

                if (canvas.toBlob) {
                    canvas.toBlob(function(blob){

                        try {
                            let blobUrl = URL.createObjectURL(blob);
                            callback(blobUrl);
                        } catch (e) {
                            console.log(e.name);
                            console.log(e.message);
                            history.back();
                        }

                    }, "image/jpeg", 1);
                }else {

                    try {
                        callback( dataURLtoBlob(canvas.toDataURL("image/jpeg",1)) );
                    } catch (e) {
                        console.log(e.name);
                        console.log(e.message);
                        history.back();
                    }

                }


            }

            img.src = file;
        }
        //tab End


    }
}
