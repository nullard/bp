import Vue from 'vue'
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export function createRouter () {
    return new VueRouter({
        mode: 'history',
        scrollBehavior: (to, from, savedPosition) => {


            if (from.name === 'image.rate' && to.name === 'profile.images') {
                return savedPosition || { x: 0, y: 0 };
            }

            return { x: 0, y: 0 };
        },
        routes:[
            { path: '/', component: require('../components/pages/Home.vue'), name:'home', meta: {keepAlive: true, auth: false} },
            { path: '/login/:recommand?', component: require('../components/pages/Login.vue'), name:'login', meta: {keepAlive: false, auth: false} },
            { path: '/forgot', component: require('../components/pages/Forgot.vue'), name:'forgot', meta: {keepAlive: false, auth: false} },
            { path: '/register', component: require('../components/pages/Register.vue'), name:'register', meta: {keepAlive: false, auth: false} },
            { path: '/signup', component: require('../components/pages/Signup.vue'), name: 'signup', meta: {keepAlive: false, auth: true} },
            { path: '/image/rate/:unique', component: require('../components/pages/ImageRate.vue'), name: 'image.rate', meta: {keepAlive: false, auth: true}, props: (route) => ({ query: route.query.q }) },
            { path: '/image/show/:unique', component: require('../components/pages/ImageShow.vue'), name: 'image.show', meta: {keepAlive: false, auth: false}, props: (route) => ({ query: route.query.q }) },
            { path: '/create', component: require('../components/pages/create_images/CreateImage.vue'), name: 'create', meta: {keepAlive: false, auth: true} },
            { path: '/create/type', component: require('../components/pages/create_images/CreateImageType.vue'), name: 'create.type', meta: {keepAlive: false, auth: true} },
            { path: '/create/content', component: require('../components/pages/create_images/CreateImageContent.vue'), name: 'create.content', meta: {keepAlive: false, auth: true} },
            { path: '/create/privacy', component: require('../components/pages/create_images/CreateImagePrivacy.vue'), name: 'create.privacy', meta: {keepAlive: false, auth: true} },
            { path: '/search', component: require('../components/pages/Search.vue'), meta: {keepAlive: true, auth: false} },
            { path: '/notice', component: require('../components/pages/Notice.vue'), name: 'notice',meta: {keepAlive: false, auth: true} },
            { path: '/setting', component: require('../components/pages/Setting.vue'), name: 'setting',meta: {keepAlive: true, auth: true} },
            { path: '/profile/edit', component: require('../components/pages/ProfileEdit.vue'), name:'profile.edit', meta: {keepAlive: false, auth: true}},
            { path: '/privacy/edit', component: require('../components/pages/ProfilePrivacyEdit.vue'), name:'profile.privacy.edit', meta: {keepAlive: false, auth: true}},
            { path: '/profile', component: require('../components/pages/Profile.vue'),
                name: 'profile',
                children: [
                    {
                        path: 'info',
                        component: require('../components/pages/profiles/ProfileInfo.vue'),
                        children: [
                            {
                                path: 'data/:unique?',
                                component: require('../components/pages/profiles/profile_infos/Index.vue'),
                                name: 'profile.info.data',
                                meta: {
                                    keepAlive: false,
                                    auth: true
                                }
                            },
                            {
                                path: 'chart/:unique?',
                                component: require('../components/pages/profiles/profile_infos/Chart.vue'),
                                name: 'profile.info.chart',
                                meta: {
                                    keepAlive: false,
                                    auth: true
                                }
                            }
                        ]
                    },
                    {
                        path: 'images/:unique?',
                        component: require('../components/pages/profiles/ProfileImages.vue'),
                        name: 'profile.images',
                        meta: {
                            keepAlive: false,
                            auth: true
                        }
                    },
                    {
                        path: 'friend-images/:unique?',
                        component: require('../components/pages/profiles/ProfileFriendImages.vue'),
                        name:'profile.friend-images',
                        meta: {
                            keepAlive: false,
                            auth: true
                        }
                    },
                    {
                        path: 'tracks/:unique?',
                        component: require('../components/pages/profiles/ProfileTracks.vue'),
                        name:'profile.tracks',
                        meta: {
                            keepAlive: false,
                            auth: true
                        }
                    },
                ]
            },
        ]
    })
}