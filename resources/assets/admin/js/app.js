
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// 引入 vuex
import Vuex from 'vuex'
Vue.use(Vuex)

window.moment = require('moment');
require('moment/locale/zh-tw');
moment.locale('zh-tw');

//https://vuematerial.io/getting-started
import VueMaterial from 'vue-material';
Vue.use(VueMaterial)

const app = new Vue({
    data: {
    },
    mounted() {},
    el: '#app'
});
