let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/framework7-icons/css/framework7-icons.css','public/css/framework7-icon/');

mix.js('resources/assets/admin/js/app.js','public/js/admin')
    .js('resources/assets/js/app.js', 'public/js')
    .extract(['axios', 'vue', 'vuex', 'jquery', 'moment', 'vue-router', 'chart.js', 'hchs-vue-charts', 'vue-material'],'public/js/vendor.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .combine(['node_modules/vue-material/dist/vue-material.min.css','node_modules/vue-material/dist/theme/default.css'],'public/css/vue-material/all.css')
    .copy('node_modules/exif-js/exif.js','public/js/plugins/exif.js');

mix.version();